import 'dart:async';
import 'dart:convert';

import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';
// import 'package:sistemts/view_models/bloc/bloc_login/bloc_login_bloc.dart';
// import 'package:sistemts/views/admin/pages/menu.dart';
// import 'package:sistemts/views/pages/login.dart';
// import 'package:sistemts/views/utils/colors.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sistemregist/views/pages/home/main.dart';
import 'package:sistemregist/views/pages/login/main.dart';
import 'package:sistemregist/views/pages_admin/home/main.dart';
import 'package:sistemregist/views/pages_dokter/home/main.dart';
import 'package:sistemregist/views/utils/colors.dart';
import 'package:sistemregist/views_models/bloc/bloc_login/login_bloc.dart';


// import 'views/pages/menu.dart';

class OPSplashScreen extends StatefulWidget {
  final bool? tandaUpdate;
  static String tag = '/OPSplashScreen';

  OPSplashScreen({this.tandaUpdate});
  @override
  _OPSplashScreenState createState() => _OPSplashScreenState();
}

class _OPSplashScreenState extends State<OPSplashScreen>
    with SingleTickerProviderStateMixin {
  String? kode;
  String? tokens;
  bool? loginStatus;
  bool? statusTanda;
  int? role;
  LoginBloc bloc = LoginBloc();
  int? myIdAkun;

  @override
  void initState() {
    super.initState();
    getRole();
    izin();
    bloc..add(BlocCekToken());
  }

  Future izin() async {
    WidgetsFlutterBinding.ensureInitialized();
    Map<Permission, PermissionStatus> statuses = await [
      Permission.storage,
    ].request();
    print(statuses[Permission.storage]);
  }

  getIzinFileExplorer()async{
    // // Pick an image
    // final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    // // Capture a photo
    // final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    // // Pick a video
    // final XFile? image = await _picker.pickVideo(source: ImageSource.gallery);
    // // Capture a video
    // final XFile? video = await _picker.pickVideo(source: ImageSource.camera);
    // // Pick multiple images
    // final List<XFile>? images = await _picker.pickMultiImage();
  }

  getRole() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      role= preferences.getInt("role");
    });
  }

  hapusToken()async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
      preferences.setString("token","");
  }

  startTime() async {
    var _duration = Duration(seconds: 2);
    return Timer(_duration, statustohome);
  }

  startTimeUser() async {
    var _duration = Duration(seconds: 2);
    return Timer(_duration, statustohomeUser);
  }

  startTimeDokter() async {
    var _duration = Duration(seconds: 2);
    return Timer(_duration, statustohomeDokter);
  }

  startTimeLogin() async {
    var _duration = Duration(seconds: 2);
    return Timer(_duration, statuslogin);
  }


  // void navigationPage()async {
  //   Navigator.pop(context);
  //   Navigator.push(
  //     context,
  //     MaterialPageRoute(
  //       builder: (context) => LoginMain(),
  //     ),
  //   );
  // }

  Future<String?> statuslogin() async {
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => LoginPage(),
      ),
    );
  }

  Future<String?> statustohome() async {
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => HomePageAdmin(),
      ),
    );
  }

  Future<String?> statustohomeDokter() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    myIdAkun = preferences.getInt("id_akun");
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => HomePageDokter(myIdAkun: myIdAkun,),
      ),
    );
  }

  Future<String?> statustohomeUser() async {
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => HomePage(),
      ),
    );
  }

  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
        body: BlocListener<LoginBloc, LoginState>(
          bloc: bloc,
          listener: (context, state) {
            print(state);
            // startTimeLogin();
            // TODO: implement listener
            if(state is BlocStateCekTokenSukses){
              print(role);
              // hapusToken();
              if(role == 3){
                startTimeUser();
              }else if(role == 2) {
                startTimeDokter();
              }else if(role == 1){
                startTime();
              }
            }
            if(state is BlocaStateCekTokenFailed){
              print("masuk");
              startTimeLogin();
            }
          },
          child: Container(
            child: BlocBuilder<LoginBloc, LoginState>(
              bloc:bloc,
              builder: (context, state) {
                print(state);
                if(state is BlocStateCekTokenSukses){
                  return myBody(size);
                }
                if(state is BlocStateLoading){
                  return myBody(size);
                }
                if(state is BlocaStateCekTokenFailed){
                  return myBody(size);
                }
                return myBody(size);
              },
            ),
          ),
        )
    );
  }

  Container myBody(Size size) {
    return Container(
      width: size.width,
      height: size.height,
      decoration: BoxDecoration(
        color: background,),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          new Image.asset("assets/logo/logo.jpeg", width: 100),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "PROLANIS",
                style: TextStyle(
                    fontSize: 45,
                    fontFamily: 'RobotoCondensed',
                    color: Colors.white),
              ),
            ],
          )
        ],
      ),
    );
  }
}
