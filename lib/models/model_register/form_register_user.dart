class registUser {
  int? id;
  String? username;
  String? name;
  String? password;
  String? currentMessage;
  String? time;
  String? icon;
  int? role;
  int? statusCurrentMessage;
  String? tglLahir;
  String? alamat;
  String? jenisKelamin;
  String? email;
  int? idAkun;

  registUser(
      {this.id,
      this.username,
      this.name,
      this.password,
      this.currentMessage,
      this.time,
      this.icon,
      this.role,
      this.statusCurrentMessage,
      this.tglLahir,
      this.alamat,
      this.jenisKelamin,
      this.idAkun,
      this.email});

  registUser.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    name = json['name'];
    password = json['password'];
    currentMessage = json['current_message'];
    time = json['time'];
    icon = json['icon'];
    role = json['role'];
    statusCurrentMessage = json['status_current_message'];
    tglLahir = json['tgl_lahir'];
    alamat = json['alamat'];
    jenisKelamin = json['jenis_kelamin'];
    idAkun = json['id_akun'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['name'] = this.name;
    data['password'] = this.password;
    data['current_message'] = this.currentMessage;
    data['time'] = this.time;
    data['icon'] = this.icon;
    data['role'] = this.role;
    data['status_current_message'] = this.statusCurrentMessage;
    data['tgl_lahir'] = this.tglLahir;
    data['alamat'] = this.alamat;
    data['jenis_kelamin'] = this.jenisKelamin;
    data['email'] = this.email;
    data['id_akun'] = this.idAkun;
    return data;
  }
}