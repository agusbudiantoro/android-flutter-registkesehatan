class ModelResponseLogin {
  int? idAkun;
  int? idPengguna;
  String? username;
  String? name;
  String? currentMessage;
  String? time;
  String? icon;
  int? role;
  int? statusCurrentMessage;
  String? tglLahir;
  String? alamat;
  String? jenisKelamin;
  String? email;
  String? token;
  int? status_regist;
  int? id;
  String? file;

  ModelResponseLogin(
      {this.idAkun,
      this.idPengguna,
      this.file,
      this.username,
      this.name,
      this.currentMessage,
      this.time,
      this.icon,
      this.role,
      this.statusCurrentMessage,
      this.tglLahir,
      this.alamat,
      this.jenisKelamin,
      this.email,
      this.status_regist,
      this.id,
      this.token});

  ModelResponseLogin.fromJson(Map<String, dynamic> json) {
    id=json['id'];
    idAkun = json['id_akun'];
    idPengguna = json['id_pengguna'];
    username = json['username'];
    name = json['name'];
    currentMessage = json['current_message'];
    time = json['time'];
    icon = json['icon'];
    role = json['role'];
    file = json['file'];
    statusCurrentMessage = json['status_current_message'];
    tglLahir = json['tgl_lahir'];
    alamat = json['alamat'];
    jenisKelamin = json['jenis_kelamin'];
    email = json['email'];
    token = json['token'];
    status_regist = json['status_regist'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_akun'] = this.idAkun;
    data['file']=this.file;
    data['id']=this.id;
    data['id_pengguna'] = this.idPengguna;
    data['username'] = this.username;
    data['name'] = this.name;
    data['current_message'] = this.currentMessage;
    data['time'] = this.time;
    data['icon'] = this.icon;
    data['role'] = this.role;
    data['status_current_message'] = this.statusCurrentMessage;
    data['tgl_lahir'] = this.tglLahir;
    data['alamat'] = this.alamat;
    data['jenis_kelamin'] = this.jenisKelamin;
    data['email'] = this.email;
    data['token'] = this.token;
    data['status_regist'] = this.status_regist;
    return data;
  }
}