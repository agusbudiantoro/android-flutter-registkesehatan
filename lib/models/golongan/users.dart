import 'dart:io';

class UserGolonganModel {
  int? id;
  String? name;
  String? currentMessage;
  String? time;
  String? icon;
  int? statusCurrentMessage;
  String? tglLahir;
  String? alamat;
  String? jenisKelamin;
  String? email;
  int? golongan;
  int? reguKelompok;
  int? statusRegist;
  File? myFile;
  String? file;

  UserGolonganModel(
      {this.id,
      this.name,
      this.currentMessage,
      this.time,
      this.icon,
      this.statusCurrentMessage,
      this.tglLahir,
      this.alamat,
      this.jenisKelamin,
      this.email,
      this.golongan,
      this.reguKelompok,
      this.file,
      this.myFile,
      this.statusRegist});

  UserGolonganModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    currentMessage = json['current_message'];
    time = json['time'];
    icon = json['icon'];
    statusCurrentMessage = json['status_current_message'];
    tglLahir = json['tgl_lahir'];
    alamat = json['alamat'];
    jenisKelamin = json['jenis_kelamin'];
    email = json['email'];
    golongan = json['golongan'];
    reguKelompok = json['regu_kelompok'];
    statusRegist = json['status_regist'];
    file = json['file'];
    myFile =json['my_file'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['current_message'] = this.currentMessage;
    data['time'] = this.time;
    data['icon'] = this.icon;
    data['status_current_message'] = this.statusCurrentMessage;
    data['tgl_lahir'] = this.tglLahir;
    data['alamat'] = this.alamat;
    data['jenis_kelamin'] = this.jenisKelamin;
    data['email'] = this.email;
    data['golongan'] = this.golongan;
    data['regu_kelompok'] = this.reguKelompok;
    data['status_regist'] = this.statusRegist;
    data['file'] = this.file;
    data['my_file'] = this.myFile;
    return data;
  }
}