class ChatModel{
  String? name;
  String? icon;
  bool? isGroup;
  String? time;
  String? currentMessages;
  String? status;
  bool select = false;
  int? id;
  int? idAkun;
  ChatModel({this.name, this.icon, this.isGroup, this.time, this.currentMessages, this.select=false, this.status, this.id, this.idAkun});
  
}