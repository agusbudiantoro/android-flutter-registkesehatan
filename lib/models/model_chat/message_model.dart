// class MessageModel{
//   String? type;
//   String? message;
//   String? time;
//   int? id;
//   int? idUserTarget;
//   int? idUser;
//   String? pesan;
//   String? waktu;
//   int? status;
//   MessageModel({this.type, this.message, this.time,this.id,
//       this.idUserTarget,
//       this.idUser,
//       this.pesan,
//       this.waktu,
//       this.status});
// }

class MessageModel {
  int? id;
  int? idUserTarget;
  int? idUser;
  String? pesan;
  String? waktu;
  int? status;

  MessageModel(
      {this.id,
      this.idUserTarget,
      this.idUser,
      this.pesan,
      this.waktu,
      this.status});

  MessageModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idUserTarget = json['id_user_target'];
    idUser = json['id_user'];
    pesan = json['pesan'];
    waktu = json['waktu'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_user_target'] = this.idUserTarget;
    data['id_user'] = this.idUser;
    data['pesan'] = this.pesan;
    data['waktu'] = this.waktu;
    data['status'] = this.status;
    return data;
  }
}