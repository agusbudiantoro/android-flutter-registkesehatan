import 'package:flutter/material.dart';
import 'package:sistemregist/splash.dart';
import 'package:sistemregist/views/utils/colors.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PROLANIS',
      theme: ThemeData(
        primaryColorDark: background,
      ),
      home: OPSplashScreen(),
    );
  }
}
