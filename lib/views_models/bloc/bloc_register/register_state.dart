part of 'register_bloc.dart';

@immutable
abstract class RegisterState {}

class RegisterInitial extends RegisterState {}

class BlocStateSukses extends RegisterState{
  final String? pesan;
    BlocStateSukses({this.pesan});
}

class BlocaStateFailed extends RegisterState{
  final String? errorMessage;
  BlocaStateFailed({this.errorMessage});
}

class BlocStateLoading extends RegisterState{

}
