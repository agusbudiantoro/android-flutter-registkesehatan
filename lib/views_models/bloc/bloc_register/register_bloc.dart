import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sistemregist/models/model_register/form_register_user.dart';
import 'package:sistemregist/views_models/api/register/api_register.dart';


part 'register_event.dart';
part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  RegisterBloc() : super(RegisterInitial());

  @override
  Stream<RegisterState> mapEventToState(
    RegisterEvent event,
  ) async* {
    if(event is BlocEventRegister){
      print("cek");
      yield* _register(event.myData!);
    }
  }
}

Stream<RegisterState> _register(registUser data)async*{
  yield BlocStateLoading();
  try {
    bool resdata = await RegisterApi.registerAkun(data);
    if(resdata == true){
      yield BlocStateSukses(); 
    }else {
      yield BlocaStateFailed();
    }
  } catch (e) {
    yield BlocaStateFailed(errorMessage: e.toString());
  }
}