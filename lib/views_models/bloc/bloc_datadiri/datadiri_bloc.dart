import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sistemregist/models/golongan/users.dart';
import 'package:sistemregist/views_models/api/api_users/api_users.dart';

part 'datadiri_event.dart';
part 'datadiri_state.dart';

class DatadiriBloc extends Bloc<DatadiriEvent, DatadiriState> {
  DatadiriBloc() : super(DatadiriInitial());

  @override
  Stream<DatadiriState> mapEventToState(
    DatadiriEvent event,) async* {
      
      if(event is EventEditDataDiri){
        yield* editDataDiri(event.data);
      }
      if(event is EventUploadFile){
        yield* uploadFile(event.data);
      }

    }
}

Stream<DatadiriState> uploadFile(data)async*{
  yield StateUploadFileLoading();
  try {
    bool resdata = await UsersApi.uploadFile(data);
    if(resdata == true){
      yield StateUploadFileSukses(status: resdata);
    }else{
      yield StateUploadFileFailed(errorMessage: "error");
    }
  } catch (e) {
    yield StateUploadFileFailed(errorMessage: e.toString());
  }
}

Stream<DatadiriState> editDataDiri(data)async*{
  yield StateEditDataDiriLoading();
  try {
    bool resdata = await UsersApi.putDataDIri(data);
    if(resdata == true){
      yield StateEditDataDiriSukses(status: resdata);
    }else{
      yield StateEditDataDiriFailed(errorMessage: "error");
    }
  } catch (e) {
    yield StateEditDataDiriFailed(errorMessage: e.toString());
  }
}