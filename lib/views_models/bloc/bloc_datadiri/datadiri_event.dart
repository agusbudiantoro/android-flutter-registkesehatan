part of 'datadiri_bloc.dart';

@immutable
abstract class DatadiriEvent {}

class EventEditDataDiri extends DatadiriEvent {
  final UserGolonganModel? data;
  EventEditDataDiri({this.data});
}

class EventUploadFile extends DatadiriEvent {
  final UserGolonganModel? data;
  EventUploadFile({this.data});
}