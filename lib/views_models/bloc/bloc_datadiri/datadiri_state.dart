part of 'datadiri_bloc.dart';

@immutable
abstract class DatadiriState {}

class DatadiriInitial extends DatadiriState {}

class StateEditDataDiriSukses extends DatadiriState {
  final bool? status;
  StateEditDataDiriSukses({this.status});
}

class StateEditDataDiriFailed extends DatadiriState {
  final String? errorMessage;
  StateEditDataDiriFailed({this.errorMessage});
}

class StateEditDataDiriLoading extends DatadiriState {
  
}

class StateUploadFileSukses extends DatadiriState {
  final bool? status;
  StateUploadFileSukses({this.status});
}

class StateUploadFileFailed extends DatadiriState {
  final String? errorMessage;
  StateUploadFileFailed({this.errorMessage});
}

class StateUploadFileLoading extends DatadiriState {
  
}