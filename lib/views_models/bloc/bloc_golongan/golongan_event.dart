part of 'golongan_bloc.dart';

@immutable
abstract class GolonganEvent {}

// get golongan by id

class EventGetGolonganById extends GolonganEvent {
  final int? id;
  EventGetGolonganById({this.id});
}

// put golongan by id

class EventPutGolonganById extends GolonganEvent {
  final UserGolonganModel? data;
  EventPutGolonganById({this.data});
}

// reset golongan by id

class EventResetGolonganById extends GolonganEvent {
  final UserGolonganModel? data;
  EventResetGolonganById({this.data});
}

// verif regist by id

class EventVerifRegistById extends GolonganEvent {
  final UserGolonganModel? data;
  EventVerifRegistById({this.data});
}