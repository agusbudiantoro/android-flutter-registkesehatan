import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sistemregist/models/golongan/users.dart';
import 'package:sistemregist/views_models/api/golongan/api_golongan.dart';

part 'golongan_event.dart';
part 'golongan_state.dart';

class GolonganBloc extends Bloc<GolonganEvent, GolonganState> {
  GolonganBloc() : super(GolonganInitial());

  @override
  Stream<GolonganState> mapEventToState(
    GolonganEvent event,
  ) async* {
    if(event is EventGetGolonganById){
      print("cekk");
      yield* getGolonganById(event.id);
    }
    if(event is EventPutGolonganById){
      yield* putGolonganById(event.data);
    }
    if(event is EventResetGolonganById){
      print("cekk");
      yield* resetGolonganById(event.data);
    }
    if(event is EventVerifRegistById){
      print("cekk");
      yield* verifGolonganById(event.data);
    }
  }
}

Stream<GolonganState> verifGolonganById(data)async*{

  yield StateVerifGolonganByIdLoading();
  try {
    UserGolonganModel myData = data;
    print("masuk");
      bool value = await GolonganApi.verifRegist(myData);
      if(value == true){
        yield StateVerifGolonganByIdSukses(status: value);
      }else {
        yield StateVerifGolonganByIdFailed(errorMessage: "gagal");
      }
  } catch (e) {
    yield StateVerifGolonganByIdFailed(errorMessage: "gagal verif");
  }
}

Stream<GolonganState> resetGolonganById(data)async*{

  yield StateResetGolonganByIdLoading();
  try {
    UserGolonganModel myData = data;
    print("masuk");
      bool value = await GolonganApi.resetGolongan(myData);
      if(value == true){
        yield StateResetGolonganByIdSukses(status: value);
      }else {
        yield StateResetGolonganByIdFailed(errorMessage: "gagal");
      }
  } catch (e) {
    yield StateResetGolonganByIdFailed(errorMessage: "gagal edit");
  }
}

Stream<GolonganState> putGolonganById(data)async*{

  yield StatePutGolonganByIdLoading();
  try {
    UserGolonganModel myData = data;
    print("masuk");
      bool value = await GolonganApi.putGolongan(myData);
      if(value == true){
        yield StatePutGolonganByIdSukses(status: value);
      }else {
        yield StatePutGolonganByIdFailed(errorMessage: "gagal");
      }
  } catch (e) {
    yield StatePutGolonganByIdFailed(errorMessage: "gagal edit");
  }
}


Stream<GolonganState> getGolonganById(id)async*{
  yield StateGetGolonganByIdLoading();
  try {
    print("masuk");
      dynamic value = await GolonganApi.getUsersGolonganById(id);
      print(value);
      var hasil = jsonDecode(value);
      UserGolonganModel hasilConvert = UserGolonganModel.fromJson(hasil);
    yield StateGetGolonganByIdSukses(data: hasilConvert);
  } catch (e) {
    yield StateGetGolonganByIdFailed(errorMessage: "gagal get");
  }
}