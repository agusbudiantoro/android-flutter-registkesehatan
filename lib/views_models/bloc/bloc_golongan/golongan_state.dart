part of 'golongan_bloc.dart';

@immutable
abstract class GolonganState {}

class GolonganInitial extends GolonganState {}

// get golongan by id

class StateGetGolonganByIdSukses extends GolonganState {
  final UserGolonganModel? data;
  StateGetGolonganByIdSukses({this.data});
}

class StateGetGolonganByIdLoading extends GolonganState {
  StateGetGolonganByIdLoading();
}

class StateGetGolonganByIdFailed extends GolonganState {
  final String? errorMessage;
  StateGetGolonganByIdFailed({this.errorMessage});
}

// put golongan by id

class StatePutGolonganByIdSukses extends GolonganState {
  final bool? status;
  StatePutGolonganByIdSukses({this.status});
}

class StatePutGolonganByIdLoading extends GolonganState {
  StatePutGolonganByIdLoading();
}

class StatePutGolonganByIdFailed extends GolonganState {
  final String? errorMessage;
  StatePutGolonganByIdFailed({this.errorMessage});
}

// reset golongan by id

class StateResetGolonganByIdSukses extends GolonganState {
  final bool? status;
  StateResetGolonganByIdSukses({this.status});
}

class StateResetGolonganByIdLoading extends GolonganState {
  StateResetGolonganByIdLoading();
}

class StateResetGolonganByIdFailed extends GolonganState {
  final String? errorMessage;
  StateResetGolonganByIdFailed({this.errorMessage});
}

//verif regist

class StateVerifGolonganByIdSukses extends GolonganState {
  final bool? status;
  StateVerifGolonganByIdSukses({this.status});
}

class StateVerifGolonganByIdLoading extends GolonganState {
  StateVerifGolonganByIdLoading();
}

class StateVerifGolonganByIdFailed extends GolonganState {
  final String? errorMessage;
  StateVerifGolonganByIdFailed({this.errorMessage});
}