import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sistemregist/models/golongan/users.dart';
import 'package:sistemregist/models/model_login/model_respone_login.dart';
import 'package:sistemregist/views_models/api/api_users/api_users.dart';

part 'akun_event.dart';
part 'akun_state.dart';

class AkunBloc extends Bloc<AkunEvent, AkunState> {
  AkunBloc() : super(AkunInitial());

  @override
  Stream<AkunState> mapEventToState(AkunEvent event,)async*{
    if(event is EventGetAllAkun){
      yield* _getAllUser();
    }
    if(event is EventGetAkunByStatus){
      yield* _getUserByStatus(event.data);
    }
    if(event is EventDeleteAkun){
      yield* _deleteUser(event.data);
    }
  }
}

Stream<AkunState> _getUserByStatus(mydata)async*{
  yield StateGetAkunByStatusLoading();
  try {
    dynamic data = await UsersApi.getUserByStatus(mydata);
    print(data);
    var listData = jsonDecode(data) as List;
    // print(listData);
    var hasilFinal = listData.map<ModelResponseLogin>((item) => ModelResponseLogin.fromJson(item)).toList();
    List<ModelResponseLogin> hasilConvert=hasilFinal;
    yield StateGetAkunByStatusSukses(data: hasilConvert); 
  } catch (e) {
    yield StateGetAkunByStatusFailed(errorMessage: e.toString());
  }
}

Stream<AkunState> _deleteUser(data)async*{
  yield StateDeleteAkunLoading();
  try {
    bool value = await UsersApi.deleteAkun(data);
    if(value == true){
      yield StateDeleteAkunSukses(status: value);
    }else{
      yield StateDeleteAkunFailed(errorMessage: "gagal hapus data");
    }
  } catch (e) {
    yield StateDeleteAkunFailed(errorMessage: e.toString());
  }
}

Stream<AkunState> _getAllUser()async*{
  yield StateGetAllAkunLoading();
  try {
    dynamic data = await UsersApi.getAllUser();
    print(data);
    var listData = jsonDecode(data) as List;
    // print(listData);
    var hasilFinal = listData.map<ModelResponseLogin>((item) => ModelResponseLogin.fromJson(item)).toList();
    List<ModelResponseLogin> hasilConvert=hasilFinal;
    yield StateGetAllAkunSukses(data: hasilConvert); 
  } catch (e) {
    yield StateGetAllAkunFailed(errorMessage: e.toString());
  }
}