part of 'akun_bloc.dart';

@immutable
abstract class AkunState {}

class AkunInitial extends AkunState {}

class StateGetAllAkunSukses extends AkunState {
  final List<ModelResponseLogin>? data;
  StateGetAllAkunSukses({this.data});
}

class StateGetAllAkunLoading extends AkunState {
  
}

class StateGetAllAkunFailed extends AkunState {
  final String? errorMessage;
  StateGetAllAkunFailed({this.errorMessage});
}

class StateDeleteAkunSukses extends AkunState {
  final bool? status;
  StateDeleteAkunSukses({this.status});
}

class StateDeleteAkunLoading extends AkunState {
  
}

class StateDeleteAkunFailed extends AkunState {
  final String? errorMessage;
  StateDeleteAkunFailed({this.errorMessage});
}

class StateGetAkunByStatusSukses extends AkunState {
  final List<ModelResponseLogin>? data;
  StateGetAkunByStatusSukses({this.data});
}

class StateGetAkunByStatusLoading extends AkunState {
  
}

class StateGetAkunByStatusFailed extends AkunState {
  final String? errorMessage;
  StateGetAkunByStatusFailed({this.errorMessage});
}