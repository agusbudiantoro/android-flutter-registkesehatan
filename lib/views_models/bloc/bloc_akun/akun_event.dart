part of 'akun_bloc.dart';

@immutable
abstract class AkunEvent {}

class EventGetAllAkun extends AkunEvent {
  
}

class EventDeleteAkun extends AkunEvent {
  final UserGolonganModel? data;
  EventDeleteAkun({this.data});
}

class EventGetAkunByStatus extends AkunEvent {
  final UserGolonganModel? data;
  EventGetAkunByStatus({this.data});
}