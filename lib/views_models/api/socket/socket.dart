import 'dart:convert';

import 'package:sistemregist/models/model_register/form_register_user.dart';
import 'package:sistemregist/views_models/domain.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

IO.Socket? mySocket= IO.io(urlDomain,<String,dynamic>{
      "transports":["websocket"],
      "autoConnect":false,
    });