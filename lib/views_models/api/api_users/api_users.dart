import 'dart:convert';
import 'dart:io';

import 'package:http/io_client.dart';
import 'package:sistemregist/models/golongan/users.dart';
import 'package:sistemregist/views_models/domain.dart';
import 'package:http/http.dart' as client;
// import 'package:http/io_client.dart';
import 'package:http_parser/http_parser.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';

const url = urlDomain+"/controller/api/v1/";
const urlAkun = urlDomain+"/akun/api/v1/";

class UsersApi {

  static Future<dynamic> getUserByStatus(data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    UserGolonganModel convertData = data;
    try {
      var response = await ioClient.get(Uri.parse(urlAkun+'getUserByStatus/'+convertData.statusRegist.toString()));
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception(e.toString());
    }
  }

  static Future<bool> deleteAkun(data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    UserGolonganModel convertData = data;
    print("cek golongan");
    print(convertData.golongan);
    try {
      print("cek");
      var response = await ioClient.delete(Uri.parse(urlAkun+'deleteAkun/'+convertData.id.toString()));
      print(response.body);
      if(response.statusCode == 200){
        return true;
      }else {
        return false;
      }
    } catch (e) {
      print(e);
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<dynamic> getAllUser()async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.get(Uri.parse(urlAkun+'getAllAkun'));
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception(e.toString());
    }
  }

    static Future<bool> putDataDIri(data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    UserGolonganModel convertData = data;
    print("cek golongan");
    print(convertData.golongan);
    Map<String, String>body={
      'alamat':convertData.alamat.toString(),
      'tgl_lahir':convertData.tglLahir.toString(),
      'jenis_kelamin':convertData.jenisKelamin.toString(),
      'email':convertData.email.toString()
    };
    try {
      print("cek");
      var response = await ioClient.put(Uri.parse(url+'editDataDiri/'+convertData.id.toString()), body: body);
      print(response.body);
      if(response.statusCode == 200){
        return true;
      }else {
        return false;
      }
    } catch (e) {
      print(e);
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<bool> uploadFile(data)async{
    UserGolonganModel myData = data;
    try {
    String fileName = basename(myData.myFile!.path);
    String base64Image = base64Encode(myData.myFile!.readAsBytesSync());
    var stream = new client.ByteStream(DelegatingStream.typed(myData.myFile!.openRead()));
    var length = await myData.myFile!.length();
    var request = client.MultipartRequest("PUT", Uri.parse(url+'uploadFile/'+myData.id.toString()));
    request.headers.addAll({
      'Content-type': 'multipart/form-data',
      'Accept':'*/*'
    });
    var pic = await client.MultipartFile("file", stream, length,filename: fileName, contentType: MediaType("multipart/form-data","multipart/form-data"));
    request.files.add(pic);
    client.Response response = await client.Response.fromStream(await request.send());
    if(response.statusCode == 200){
      return true;
    }else {
      return false;
    }
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }
}