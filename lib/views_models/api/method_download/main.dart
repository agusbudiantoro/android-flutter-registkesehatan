import 'package:dio/dio.dart';
import 'package:external_path/external_path.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sistemregist/views_models/domain.dart';
import 'package:sn_progress_dialog/sn_progress_dialog.dart';

const url = urlDomain+"/controller/api/v1/download/";

class DownloadFile extends StatefulWidget {
  String? filename;
  String? title;
  DownloadFile({ this.title, this.filename});

  @override
  _DownloadFileState createState() => _DownloadFileState();
}

class _DownloadFileState extends State<DownloadFile> {
  int? progress;
  double percentage = 0.0;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    // downloadMethod(widget.context!, widget.url, widget.title);
  }


  Future downloadMethod(BuildContext context, filename,String title) async {
    print("sinii");
    print(url+filename);
    print("cek title");
    print(title);
    ProgressDialog pr;
    pr = new ProgressDialog(context: context);
    // try {
    pr.show(max: 100, msg: 'Preparing Download...');
    Dio dio = Dio();
    String path = await ExternalPath.getExternalStoragePublicDirectory(
        ExternalPath.DIRECTORY_DOWNLOADS);
    print("cel");
    await dio.download(
      url+filename,
      '$path/$title',
      onReceiveProgress: (rec, total) {
        setState(() {
          progress = (((rec / total) * 100).toInt());
          pr.update(value: progress!, msg: "Please Wait ");
          print(progress);
          if(progress == 100){
            ScaffoldMessenger.of(context).showSnackBar(
                                          SnackBar(
                                            content: Text("Berhasil Download, File berada di folder download"),
                                            duration: Duration(milliseconds: 2500),
                                            backgroundColor: Colors.blue,
                                            onVisible: (){
                                              
                                            },
                                          )
                                        );
                                        Future.delayed(Duration(milliseconds: 2500), () => tutupPopUp(context));
          }
        });
      },
      options:
          Options(responseType: ResponseType.bytes, followRedirects: false),
      deleteOnError: true,
    );
    
  }

  tutupPopUp(BuildContext context){
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: CupertinoAlertDialog(
                title: const Text('Download'),
                content: const Text('Tetap Download ?'),
                actions: <CupertinoDialogAction>[
                  CupertinoDialogAction(
                    child: const Text('No'),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  CupertinoDialogAction(
                    child: const Text('Yes'),
                    isDestructiveAction: true,
                    onPressed: () {
                      print("cek title");
                      print(widget.title);
                     downloadMethod( context, widget.filename, widget.title!);
                      // Do something destructive.
                    },
                  )
                ],
              ),
    );
  }
}