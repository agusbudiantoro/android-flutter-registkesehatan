import 'dart:io';

import 'package:http/io_client.dart';
import 'package:sistemregist/models/model_login/model_form_login.dart';
import 'package:sistemregist/views_models/domain.dart';

const url = urlDomain+"/auth/api/v1/";

class LoginApi {
  static Future<dynamic> loginAkun(data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    ModelFormLogin convertData = data;
    Map<String, String>body={
      'username':convertData.username!,
      'password':convertData.password!
    };
    try {
      print("cek");
      var response = await ioClient.post(Uri.parse(url+'login'), body: body);
      print(response.body);
      return response.body;
    } catch (e) {
      print(e);
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<dynamic> cekTokenLogin(token)async{
    print(token.toString());
    print("diatas");
    HttpClient client1 = new HttpClient();
    client1.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client1);
    try {
      var response = await ioClient.post(Uri.parse(url+'cekToken'),headers: {
          HttpHeaders.authorizationHeader: 'Bearer $token',
        },);
        print(response.body);
      if(response.statusCode == 200){
        return response.body;
      }else {
        return throw Exception(response.body.toString());
      }
    } catch (e) {
      print(e);
      return throw Exception(e.toString());
    }
  }
}