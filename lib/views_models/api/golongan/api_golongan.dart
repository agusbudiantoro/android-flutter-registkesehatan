import 'dart:io';

import 'package:http/io_client.dart';
import 'package:sistemregist/models/golongan/users.dart';
import 'package:sistemregist/views_models/domain.dart';

const url = urlDomain+"/controller/api/v1/";
const urlUsers = urlDomain+"/akun/api/v1/";

class GolonganApi {
  static Future<dynamic> getUsersGolonganById(id)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.get(Uri.parse(url+'getUserById/'+id.toString()));
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception(e.toString());
    }
  }

  static Future<bool> putGolongan(data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    UserGolonganModel convertData = data;
    print("cek golongan");
    print(convertData.golongan);
    Map<String, String>body={
      'golongan':convertData.golongan.toString(),
      'regu_kelompok':convertData.reguKelompok.toString()
    };
    try {
      print("cek");
      var response = await ioClient.put(Uri.parse(urlUsers+'editGolongan/'+convertData.id.toString()), body: body);
      print(response.body);
      if(response.statusCode == 200){
        return true;
      }else {
        return false;
      }
    } catch (e) {
      print(e);
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<bool> resetGolongan(data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    UserGolonganModel convertData = data;
    try {
      print("cek");
      var response = await ioClient.put(Uri.parse(urlUsers+'resetGolongan/'+convertData.id.toString()));
      print(response.body);
      if(response.statusCode == 200){
        return true;
      }else {
        return false;
      }
    } catch (e) {
      print(e);
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<bool> verifRegist(data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    UserGolonganModel convertData = data;
    try {
      print("cek");
      var response = await ioClient.put(Uri.parse(urlUsers+'verifStatusRegist/'+convertData.id.toString()));
      print(response.body);
      if(response.statusCode == 200){
        return true;
      }else {
        return false;
      }
    } catch (e) {
      print(e);
      return throw Exception("Data tidak ditemukan");
    }
  }
  
}