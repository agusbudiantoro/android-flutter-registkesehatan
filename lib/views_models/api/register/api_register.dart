import 'dart:io';

import 'package:http/io_client.dart';
import 'package:sistemregist/models/model_register/form_register_user.dart';
import 'package:sistemregist/views_models/domain.dart';

const url = urlDomain+"/akun/api/v1/";

class RegisterApi {
  static Future<bool> registerAkun(data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    registUser convertData = data;
    Map<String, String>body={
      'username': convertData.username!,
      'password': convertData.password!,
      'name':convertData.name!,
      'current_message':'',
      'time':'',
      'icon':'person.svg',
      'role':'3'
    };
    try {
      print("cek");
      var response = await ioClient.post(Uri.parse(url+'createAkun'), body: body);
      if(response.statusCode == 200){
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print(e);
      return throw Exception("Data tidak ditemukan");
    }
  }
}