import 'package:flutter/material.dart';
import 'package:sistemregist/views/utils/colors.dart';

class TextFieldCustom extends StatefulWidget {
  String? hintText;
  TextEditingController? con;
  Color? fillColor;
  bool? statFill;
  bool? typePass;
  final IconData? suffixIcon;
  final bool? isObsercure;
  final VoidCallback? clickCallback;
  final IconData? prefixIcon;

  TextFieldCustom({this.hintText, this.con, this.fillColor, this.statFill, this.suffixIcon, this.clickCallback, this.isObsercure=false, this.typePass, this.prefixIcon});

  @override
  _TextFieldCustomState createState() => _TextFieldCustomState();
}

class _TextFieldCustomState extends State<TextFieldCustom> {
  @override
  Widget build(BuildContext context) {
    return Container(
              child: TextField(
                maxLines: (widget.typePass == true)?1:1,
                obscureText: widget.isObsercure!,
                style: TextStyle(color: caption),
                cursorColor: caption,
                decoration: InputDecoration(
                  
                  suffixIcon: GestureDetector(
                  onTap: () {
                    widget.clickCallback!();
                  },
                  child: Icon(
                          widget.suffixIcon,
                          color: Colors.white,
                        ),),
                      // : Container()),
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(15)),
                  hintStyle: TextStyle(color: caption, fontWeight: FontWeight.bold),
                  hintText: widget.hintText,
                  filled: widget.statFill,
                  fillColor: widget.fillColor
                ),
                controller: widget.con,

              ),
            );
  }
}