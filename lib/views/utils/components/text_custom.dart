import 'package:flutter/material.dart';

class textCustom extends StatefulWidget {
  String? text;
  Color? color;
  Alignment? align;
  double? fontSize;
  FontWeight? fontWeight;

  textCustom({ this.text, this.color, this.align, this.fontSize, this.fontWeight});

  @override
  _textCustomState createState() => _textCustomState();
}

class _textCustomState extends State<textCustom> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: widget.align,
      child:Text(widget.text!, style: TextStyle(color: widget.color, fontSize:widget.fontSize, fontWeight: widget.fontWeight),)
    );
  }
}