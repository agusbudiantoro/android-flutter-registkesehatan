import 'package:flutter/material.dart';
import 'package:sistemregist/views/pages/home/main.dart';

class ButtonCustom extends StatefulWidget {
  double? sizeContainer;
  String? textButton;
  double? borderRadius;
  Color? colorsButton;
  final VoidCallback? clickCallback;
  ButtonCustom({this.borderRadius, this.sizeContainer, this.textButton, this.colorsButton, this.clickCallback});

  @override
  _ButtonCustomState createState() => _ButtonCustomState();
}

class _ButtonCustomState extends State<ButtonCustom> {
  @override
  Widget build(BuildContext context) {
    return Container(
              width: widget.sizeContainer,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(widget.colorsButton),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(widget.borderRadius!),
                    side: BorderSide(color: Colors.transparent)
                  )
                )
                ),
                onPressed: (){
                  widget.clickCallback!();
                },
                child: Text(widget.textButton!)
                ),
            );
  }
}