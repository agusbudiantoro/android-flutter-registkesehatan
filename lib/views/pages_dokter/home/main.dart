import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sistemregist/models/model_chat/model_chat.dart';
import 'package:sistemregist/models/model_register/form_register_user.dart';
import 'package:sistemregist/views/pages/forum_chat/main.dart';
import 'package:sistemregist/views/pages/forum_chat/widget/custom_card.dart';
import 'package:sistemregist/views/pages/home/menu/main.dart';
import 'package:sistemregist/views/pages/login/main.dart';
import 'package:sistemregist/views/pages_dokter/forum_chat/widget/custom_card.dart';
import 'package:sistemregist/views/pages_dokter/home/menu/main.dart';
import 'package:sistemregist/views/utils/colors.dart';
import 'package:sistemregist/views/utils/components/text_custom.dart';
import 'package:sistemregist/views_models/api/socket/socket.dart';
import 'package:socket_io_client/src/darty.dart';
// import 'package:socket_io_client/socket_io_client.dart' as IO;

class HomePageDokter extends StatefulWidget {
  const HomePageDokter({ Key? key, this.chats, this.myIdAkun }) : super(key: key);
  final List<ChatModel>? chats;
  final int? myIdAkun;

  @override
  _HomePageDokterState createState() => _HomePageDokterState(myIdAkun: myIdAkun);
}

class _HomePageDokterState extends State<HomePageDokter> {
  _HomePageDokterState({this.myIdAkun});
  String? nama;
  int? myIdAkun;
  List<registUser> listUser=[];

   @override
  void initState() {
    // TODO: implement initState
    super.initState();
    connectSocket(myIdAkun!);
  }


  void connectSocket(int idAkun){
      mySocket!.connect();
      mySocket!.emit("signin",idAkun);
      mySocket!.onConnect((data){
        print("connected");
      });
      mySocket!.on("message", (msg){
        print("cek");
        print(msg);
        mySocket!.on("getusers", (data){
        print(data);
        print("cek");
        var convertToString =json.encode(data);
        var listData = jsonDecode(convertToString) as List;
        var hasilFinal = listData.map<registUser>((item) => registUser.fromJson(item)).toList();
        List<registUser> hasilConvert=hasilFinal;
      if(mounted){
        setState(() {
          listUser = hasilConvert;
        });
      }
      });
      mySocket!.emit("getusers",);
        // setMessage(msg['targetId'],msg["sourceId"], msg["message"]);
        // listController.animateTo(listController.position.maxScrollExtent+50, duration: Duration(milliseconds: 300), curve: Curves.easeOut);
      });
      mySocket!.on("getusers", (data){
        print(data);
        print("cek");
        var convertToString =json.encode(data);
        var listData = jsonDecode(convertToString) as List;
        var hasilFinal = listData.map<registUser>((item) => registUser.fromJson(item)).toList();
        List<registUser> hasilConvert=hasilFinal;
      if(mounted){
        setState(() {
          listUser = hasilConvert;
        });
      }
      });
      mySocket!.emit("getusers",);
    }

  void _toPage(page){
    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>page));
  }

  void logout()async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
      await preferences.remove("email");
        await preferences.remove("token");
        await preferences.remove("nama");
        await preferences.remove("username");
        await preferences.remove("role");
        await preferences.remove("tgl_lahir");
        await preferences.remove("id_pengguna");
        await preferences.remove("id_akun");
        await preferences.remove("alamat");
        await preferences.remove("jenis_kelamin");
  }
  
  @override
  Widget build(BuildContext context) {
    dynamic size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: background,
      appBar: AppBar(
        backgroundColor: background,
        title: Text("Daftar User"),
        actions: [
          IconButton(onPressed: (){
            logout();
                Navigator.pop(context);
                print("sini");
                
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>LoginPage()));
          }, icon: Icon(Icons.logout_outlined)),
        ],
      ),
      body: ListView.builder(
        itemCount: listUser.length,
        itemBuilder: (context, index)=>CustomCardDokter(chatModel:listUser[index]),
        )
    );
  }
}