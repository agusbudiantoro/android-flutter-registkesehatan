import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sistemregist/models/model_chat/model_chat.dart';
import 'package:sistemregist/models/model_register/form_register_user.dart';
import 'package:sistemregist/views/pages/forum_chat/widget/body.dart';
import 'package:sistemregist/views/pages_dokter/forum_chat/main.dart';
import 'package:sistemregist/views/utils/colors.dart';

class CustomCardDokter extends StatelessWidget {
  const CustomCardDokter({ Key? key,this.chatModel, this.sourcechat}) : super(key: key);
  final registUser? chatModel;
  final registUser? sourcechat;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        ChatModel akunPerson = ChatModel(id: chatModel?.id,idAkun: chatModel?.idAkun, name: chatModel?.name, );
        Navigator.push(context, MaterialPageRoute(builder: (context)=>ForumChatDokter(sourcechat: akunPerson,role:2)));
      },
      child: Column(
        children: [
          ListTile(
            leading: CircleAvatar(
              radius: 30,
              child: SvgPicture.asset( "assets/person.svg",color: Colors.grey[200],height: 36,width: 36,),
              backgroundColor: Colors.grey,
            ),
            title: Text(chatModel!.name!, style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: caption),),
            subtitle: Row(
              children: [
                Icon(Icons.done_all, color: caption,),
                SizedBox(width: 3,),
                Text(chatModel!.currentMessage!, style: TextStyle(fontSize: 13,color: caption),)
              ],
            ),
            trailing: Column(
              children: [
                Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(bottom: 8),
                  height: 20,
                  width: 20,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.blue
                  ),
                  child: Text("1", style: TextStyle(color: caption),),
                ),
                Text(chatModel!.time!, style: TextStyle(color: caption),),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 20, left: 80),
            child: Divider(thickness: 1),
          )
        ],
      ),
    );
  }
}