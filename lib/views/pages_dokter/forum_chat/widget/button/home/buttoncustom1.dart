import 'package:flutter/material.dart';

class button1 extends StatefulWidget {
  String? name;
  IconData? iconButton;
  Color? collorButton;
  Color? collorName;
  Color? collorIcon;
  double? borderRadius; 
  Color? fillCollor;
  double? height;
  double? width;
  double? sizeIcon;
  double? sizeText;
  final VoidCallback? clickCallback;
  button1({this.name, this.iconButton, this.collorButton, this.collorName, this.collorIcon, this.borderRadius, this.fillCollor, this.clickCallback, this.height, this.width , this.sizeIcon, this.sizeText});
  // const button1({Key? key}) : super(key: key);

  @override
  _button1State createState() => _button1State();
}

class _button1State extends State<button1> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return TextButton(
        style: ButtonStyle(
        shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0))),
      ),
        onPressed: () {
          widget.clickCallback!();
          // Navigator.push(
          //     context,
          //     MaterialPageRoute(
          //         builder: (context) => EditBarang(
          //               idBarang: listData[index].idBarang.toString(),
          //               namaBarang: listData[index].namaBarang,
          //               qty: listData[index].qty.toString(),
          //               harga: listData[index].harga.toString(),
          //               promo: listData[index].promo.toString(),
          //               hargaPromo: listData[index].hargaPromo.toString(),
          //               deskripsi: listData[index].deskripsi,
          //               kategori: listData[index].kategori.toString(),
          //             )));
        },
        child: Container(
          // padding: EdgeInsets.all(8),
          alignment: Alignment.center,
          height: widget.height,
          width: size.width / widget.width!,
          decoration: BoxDecoration(
            border: Border.all(color: widget.collorButton!, width: 2),
              color: widget.fillCollor, 
              borderRadius: BorderRadius.circular(widget.borderRadius!)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Icon(widget.iconButton, color: widget.collorIcon, size: widget.sizeIcon,),
              Text(
                widget.name!,
                style: TextStyle(color: widget.collorName,fontSize: widget.sizeText),
              )
            ],
          ),
        ));
  }
}
