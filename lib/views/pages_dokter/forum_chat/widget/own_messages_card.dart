import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sistemregist/views/utils/colors.dart';

class OwnMessagesCard extends StatefulWidget {
  const OwnMessagesCard({ Key? key, this.message, this.time }) : super(key: key);
  final String? message;
  final String? time;

  @override
  _OwnMessagesCardState createState() => _OwnMessagesCardState();
}

class _OwnMessagesCardState extends State<OwnMessagesCard> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Align(
      alignment: Alignment.centerRight,
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxWidth: size.width-45,
        ),
        child: Card(
          elevation: 1,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          color: Colors.blue,
          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
          child: Stack(
            children: [
              Padding(
                padding: EdgeInsets.only(left:10, right:60, top:5, bottom: 20),
                child: Text(widget.message!,style: TextStyle(fontSize: 16,color: caption),),
              ),
              Positioned(
                bottom: 4,
                right:10,
                child: Row(
                  children: [
                    Text(widget.time.toString(), style: TextStyle(fontSize: 13, color: Colors.white60),),
                    SizedBox(width:5),
                    Icon(Icons.done_all, size: 20,),
                  ],
                ),
              )
            ],
          ),
        ),
        ),
    );
  }
}