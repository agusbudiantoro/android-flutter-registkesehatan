import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sistemregist/views/utils/colors.dart';

class ReplyCard extends StatefulWidget {
  const ReplyCard({ Key? key, this.message, this.time }) : super(key: key);
  final String? message;
  final String? time;

  @override
  _ReplyCardState createState() => _ReplyCardState();
}

class _ReplyCardState extends State<ReplyCard> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Align(
      alignment: Alignment.centerLeft,
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxWidth: size.width-45,
        ),
        child: Card(
          elevation: 1,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          color: background,
          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
          child: Stack(
            children: [
              Padding(
                padding: EdgeInsets.only(left:10, right:60, top:5, bottom: 20),
                child: Text(widget.message!,style: TextStyle(fontSize: 16, color: caption),),
              ),
              Positioned(
                bottom: 4,
                right:10,
                child: Row(
                  children: [
                    Text(widget.time.toString(), style: TextStyle(fontSize: 13, color: Colors.white60),),
                  ],
                ),
              )
            ],
          ),
        ),
        ),
    );
  }
}