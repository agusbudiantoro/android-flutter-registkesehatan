import 'package:flutter/material.dart';
import 'package:sistemregist/models/model_chat/model_chat.dart';
import 'package:sistemregist/views/pages/forum_chat/widget/body1.dart';
import 'package:sistemregist/views/pages/forum_chat/widget/custom_card.dart';
import 'package:sistemregist/views/pages_dokter/forum_chat/widget/body1.dart';
import 'package:sistemregist/views/utils/colors.dart';

class ForumChatDokter extends StatefulWidget {
  const ForumChatDokter({ Key? key,this.chats, this.sourcechat, this.role }) : super(key: key);
  final List<ChatModel>? chats;
  final ChatModel? sourcechat;
  final int? role;

  @override
  _ForumChatDokterState createState() => _ForumChatDokterState();
}

class _ForumChatDokterState extends State<ForumChatDokter> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: background,
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        automaticallyImplyLeading: false,
        actions: [
          Container(
            padding: EdgeInsets.only(left: 15,right: 15),
            width: size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Container(
                    height: size.height/20,
                    width: size.width/10,
                    decoration: BoxDecoration(
                      border: Border.all(color: caption),
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: Icon(Icons.arrow_back_ios_new, size: 16,),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: Text(widget.sourcechat!.name.toString(), style:TextStyle(color: caption))
                ),
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: CircleAvatar(
                    child: Icon(Icons.person),
                    radius: size.width / 20,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
      body: BoxChatDokter(sourcechat: widget.sourcechat,)
    );
  }
}