import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sistemregist/models/model_login/model_form_login.dart';
import 'package:sistemregist/views/pages/home/main.dart';
import 'package:sistemregist/views/pages/register/main.dart';
import 'package:sistemregist/views/pages_admin/home/main.dart';
import 'package:sistemregist/views/pages_dokter/home/main.dart';
import 'package:sistemregist/views/utils/colors.dart';
import 'package:sistemregist/views/utils/components/button_custom.dart';
import 'package:sistemregist/views/utils/components/text_field.dart';
import 'package:sistemregist/views/utils/images.dart';
import 'package:sistemregist/views_models/bloc/bloc_login/login_bloc.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({ Key? key }) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController username = TextEditingController();
  TextEditingController pass = TextEditingController();
  LoginBloc bloc = LoginBloc();
  bool statusPass = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void _clickCallBack() {
    setState(() {
      statusPass = !statusPass;
    });
  }

  void _clickCallBackLogin() {
    print(username.text);
    print(pass.text);
    ModelFormLogin isi = ModelFormLogin(username: username.text.toString(), password: pass.text.toString());
        bloc..add(BlocEventLogin(myData: isi));
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: background,
      body: Container(
        margin: EdgeInsets.all(25),
        padding: EdgeInsets.symmetric(vertical: 60),
        color:background,
        width: size.width,
        height: size.height,
        child: Column(
          children: [
            Container(
              child: new Image.asset(loginImage1)
            ),
            SizedBox(height: size.height/20,),
            Container(
              margin: EdgeInsets.only(bottom: size.height/80),
              child: TextFieldCustom(hintText: "Username",con: username,statFill: true,fillColor: Colors.grey[800],typePass: false,prefixIcon: Icons.person,),
            ),
            Container(
              margin: EdgeInsets.only(bottom: size.height/40),
              child: TextFieldCustom(hintText: "Password",con: pass,statFill: true,typePass: true,fillColor: Colors.grey[800],prefixIcon: Icons.vpn_key_rounded,suffixIcon: Icons.remove_red_eye,isObsercure: statusPass,clickCallback: () => _clickCallBack()),
            ),
            BlocListener<LoginBloc, LoginState>(
                                    bloc: bloc,
                                    listener: (context, state) {
                                      if(state is BlocStateSukses){
                                        ScaffoldMessenger.of(context).showSnackBar(
                                          SnackBar(
                                            content: Text("Login Berhasil"),
                                            duration: Duration(milliseconds: 500),
                                            backgroundColor: Colors.green,
                                            onVisible: (){
                                              Navigator.pop(context);
                                              if(state.myData!.role == 3){
                                                Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
                                              }else if(state.myData!.role == 2) {
                                                Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePageDokter(myIdAkun: state.myData?.idAkun,)));
                                              }else if(state.myData!.role == 1) {
                                                Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePageAdmin()));
                                              }
                                              
                                            },
                                          )
                                        );
                                      }
                                      if(state is BlocaStateFailed){
                                        ScaffoldMessenger.of(context).showSnackBar(
                                          SnackBar(
                                            content: Text("Login Gagal, "+state.errorMessage.toString()),
                                            duration: Duration(milliseconds: 500),
                                            backgroundColor: Colors.red,
                                          )
                                        );
                                      }
                                    },
                                    child: Container(
                                      child: BlocBuilder<LoginBloc, LoginState>(
                                        bloc: bloc,
                                        builder: (context, state) {
                                          if (state is BlocStateSukses) {
                                            return ButtonCustom(
                                                  borderRadius: 18.0,
                                                  colorsButton: Colors.blue,
                                                  textButton: "Login",
                                                  sizeContainer:size.width,
                                                  clickCallback: () => _clickCallBackLogin()
                                                );
                                          }
                                          if (state is BlocStateLoading) {
                                            return Center(
                                              child: CircularProgressIndicator(color: Colors.white,),
                                            );
                                          }
                                          if (state is BlocaStateFailed) {
                                            return ButtonCustom(
                                                  borderRadius: 18.0,
                                                  colorsButton: Colors.blue,
                                                  textButton: "Login",
                                                  sizeContainer:size.width,
                                                  clickCallback: () => _clickCallBackLogin()
                                                );
                                          }
                                          if (state is BlocLoginInitial) {
                                            return ButtonCustom(
                                                  borderRadius: 18.0,
                                                  colorsButton: Colors.blue,
                                                  textButton: "Login",
                                                  sizeContainer:size.width,
                                                  clickCallback: () => _clickCallBackLogin()
                                                );
                                          }
                                          return ButtonCustom(
                                                  borderRadius: 18.0,
                                                  colorsButton: Colors.blue,
                                                  textButton: "Login",
                                                  sizeContainer:size.width,
                                                  clickCallback: () => _clickCallBackLogin()
                                                );
                                        },
                                      ),
                                    ),
                                  ),
            ButtonCustom(
              clickCallback: () {
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>RegisterMain()));
              },
              borderRadius: 18.0,
              colorsButton: Colors.blue,
              textButton: "Registrasi",
              sizeContainer:size.width,
            )
          ],
        ),
      ),
    );
  }
}