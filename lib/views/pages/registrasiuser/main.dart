import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_holo_date_picker/flutter_holo_date_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sistemregist/models/golongan/users.dart';
import 'package:sistemregist/views/utils/colors.dart';
import 'package:sistemregist/views/utils/components/field_register.dart';
import 'package:sistemregist/views_models/api/method_download/main.dart';
import 'package:sistemregist/views_models/bloc/bloc_datadiri/datadiri_bloc.dart';
import 'package:sistemregist/views_models/bloc/bloc_golongan/golongan_bloc.dart';

class BayarRegistrasi extends StatefulWidget {
  final int? idPengguna;

  const BayarRegistrasi({ Key? key, this.idPengguna}) : super(key: key);

  @override
  _BayarRegistrasiState createState() => _BayarRegistrasiState();
}

class _BayarRegistrasiState extends State<BayarRegistrasi> {

  DatadiriBloc bloc = DatadiriBloc();
  GolonganBloc blocGol = GolonganBloc();
  UserGolonganModel data = UserGolonganModel();
  File? myFile;
 

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("sini");
    print(widget.idPengguna);
    blocGol..add(EventGetGolonganById(id: widget.idPengguna));
  }


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      floatingActionButton: BlocListener<DatadiriBloc, DatadiriState>(
        bloc: bloc,
        listener: (context, state) {
          // TODO: implement listeneri
          if(state is StateUploadFileSukses){
            blocGol..add(EventGetGolonganById(id: widget.idPengguna));
          }
        },
        child:Container(
          child: BlocBuilder<DatadiriBloc, DatadiriState>(
            bloc: bloc,
            builder: (context, state) {
              if(state is StateUploadFileSukses){
                return floatingButton(); 
              }
              if(state is StateUploadFileLoading){
                return Container(
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          );
              }
              if(state is StateUploadFileFailed){
                return floatingButton(); 
              }
              return floatingButton();
            },
          ),
        )
      ),
      backgroundColor: background,
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        automaticallyImplyLeading: false,
        actions: [
          Container(
            padding: EdgeInsets.only(left: 15,right: 15),
            width: size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Container(
                    height: size.height/20,
                    width: size.width/10,
                    decoration: BoxDecoration(
                      border: Border.all(color: caption),
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: Icon(Icons.arrow_back_ios_new, size: 16,),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: CircleAvatar(
                    child: Icon(Icons.person),
                    radius: size.width / 20,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        child: Container(
          child: Column(
            children: [
              BlocListener<GolonganBloc, GolonganState>(
                bloc: blocGol,
                listener: (context, state) {
                  // TODO: implement listener
                  if(state is StateGetGolonganByIdSukses){
                    setState(() {
                      data = state.data!;
                    });
                  }
                },
                child: Container(
                  child: BlocBuilder<GolonganBloc, GolonganState>(
                    bloc: blocGol,
                    builder: (context, state) {
                      if(state is StateGetGolonganByIdSukses){
                        return Column(
                          children: [
                            Container(
                              child: (state.data?.statusRegist == 1)?Text("Belum Upload Pembayaran", style: TextStyle(color: Colors.red),):(state.data?.statusRegist == 2)?Text("Menunggu Verifikasi Admin", style: TextStyle(color: Colors.yellow),):(state.data?.statusRegist == 3)?Text("Pembayaran Terverifikasi", style: TextStyle(color: Colors.green),):Text(""),
                            ),
                            (state.data?.statusRegist == 2)?Container(
                              child: IconButton(
                                onPressed: (){
                                  _clickCallBack(context, state.data!.file!,state.data!.file!);
                                },
                                icon: Icon(Icons.download, color: Colors.green,)),
                            ):Container()
                          ],
                        );
                      }
                      if(state is StateGetGolonganByIdFailed){
                        return Container(
                          child: Text(state.errorMessage.toString()),
                        );
                      }
                      if(state is StateGetGolonganByIdLoading){
                        return Container(
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        );
                      }
                      return Container();
                    },
                  ),
                ),
              ),
              GestureDetector(
              onTap: (){
                openGalerry();
              },
              child: Container(
                      margin: EdgeInsets.all(5),
                      height: size.height/6,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: background,
                        borderRadius: BorderRadius.circular(20)
                      ),
                      child: (myFile == null)?Icon(Icons.upload_file, color: caption,size: size.width/6):Icon(Icons.file_present_sharp, color: caption,size: size.width/6)
                    ),
            )
            ],
          ),
        ),
      )
    );
  }

  Future<void> _clickCallBack(BuildContext context, String filename, String originalName) {
    print("download");
    return showCupertinoDialog(
              context: context,
              builder: (BuildContext context) => DownloadFile(filename: filename,title: originalName,)
            );
  }

  openGalerry()async{
    // final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    // print(image!.name);
    // print(image.readAsBytes());
    FilePickerResult? result = await FilePicker.platform.pickFiles(withData: true,withReadStream: true,type: FileType.custom,
  allowedExtensions: ['pdf','png','jpg'],);

    if (result != null) {
      print(result.paths);
      PlatformFile file = result.files.first;

      print(file.name);
      print(file.bytes);
      print(file.size);
      print(file.extension);
      print(file.path);
      setState(() {
        myFile = File(file.path!);
      });
    } else {
      // User canceled the picker
    }
  }


  FloatingActionButton floatingButton() {
    return FloatingActionButton(
      onPressed: (){
        UserGolonganModel dataModel = UserGolonganModel(id: widget.idPengguna, myFile: myFile);
        bloc..add(EventUploadFile(data: dataModel));
      },
      child: Icon(Icons.save),
    );
  }

}