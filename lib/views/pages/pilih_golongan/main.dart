import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sistemregist/models/golongan/users.dart';
import 'package:sistemregist/views/utils/colors.dart';
import 'package:sistemregist/views_models/bloc/bloc_golongan/golongan_bloc.dart';

class golongan extends StatefulWidget {
  final int? idPengguna;
  const golongan({ Key? key, this.idPengguna }) : super(key: key);

  @override
  _golonganState createState() => _golonganState();
}

class _golonganState extends State<golongan> {

  GolonganBloc bloc = GolonganBloc();
  int? pilihGol;
  int? statusRegist;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("sini");
    print(widget.idPengguna);
    bloc..add(EventGetGolonganById(id: widget.idPengguna));
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      floatingActionButton: BlocListener<GolonganBloc, GolonganState>(
        bloc: bloc,
        listener: (context, state) {
          // TODO: implement listeneri
          if(state is StatePutGolonganByIdSukses){
            bloc..add(EventGetGolonganById(id: widget.idPengguna));
          }
        },
        child: (statusRegist == 0 )?Container(
          child: BlocBuilder<GolonganBloc, GolonganState>(
            bloc: bloc,
            builder: (context, state) {
              if(state is StatePutGolonganByIdSukses){
                return floatingButton(); 
              }
              if(state is StatePutGolonganByIdLoading){
                return Container(
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          );
              }
              if(state is StatePutGolonganByIdFailed){
                return floatingButton(); 
              }
              return floatingButton();
            },
          ),
        ):Container(),
      ),
      backgroundColor: background,
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        automaticallyImplyLeading: false,
        actions: [
          Container(
            padding: EdgeInsets.only(left: 15,right: 15),
            width: size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Container(
                    height: size.height/20,
                    width: size.width/10,
                    decoration: BoxDecoration(
                      border: Border.all(color: caption),
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: Icon(Icons.arrow_back_ios_new, size: 16,),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: CircleAvatar(
                    child: Icon(Icons.person),
                    radius: size.width / 20,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        child: Container(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(15),
                child: BlocListener<GolonganBloc, GolonganState>(
                  bloc: bloc,
                  listener: (context, state) {
                    if(state is StateGetGolonganByIdSukses){
                      setState(() {
                        pilihGol = state.data?.golongan;
                        statusRegist=state.data?.statusRegist;
                      });
                    }
                  },
                  child: Container(
                    child: BlocBuilder<GolonganBloc, GolonganState>(
                      bloc: bloc,
                      builder: (context, state) {
                        if(state is StateGetGolonganByIdSukses){
                          return columnContent(size);
                        }
                        if(state is StateGetGolonganByIdLoading){
                          return Container(
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          );
                        }
                        if(state is StateGetGolonganByIdFailed){
                          return Container(
                            child: Center(
                              child: Text("gagal load data"),
                            ),
                          );
                        }
                        return columnContent(size);
                      },
                    ),
                  ),
                )
              )
            ],
          ),
        ),
      )
    );
  }

  FloatingActionButton floatingButton() {
    return FloatingActionButton(
      onPressed: (){
        UserGolonganModel dataModel = UserGolonganModel(golongan: pilihGol,reguKelompok: pilihGol,id: widget.idPengguna);
        bloc..add(EventPutGolonganById(data: dataModel));
      },
      child: Icon(Icons.save),
    );
  }

  Column columnContent(Size size) {
    return Column(
                children: [
                  (statusRegist == 0)?pilihGolongan(size):golonganUser(size),
                  SizedBox(height: 10,),
                  pilihRegu(size),
                  SizedBox(height: 10,),
                  (pilihGol == 1)?
                  jadwalDm(size):jadwalHipertensi(size)
                  
                ],
              );
  }

  Container golonganUser(Size size) {
    return Container(
                    child: Column(
                      children: [
                        Center(
                    child: Text("Golongan", style: TextStyle(color: caption, fontSize: 18),),
                    ),
                    SizedBox(height: 10,),
                    (pilihGol == 1)?Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Text("DM", style: TextStyle(color: caption,fontSize: 15),),
                    ):
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Text("Hipertensi", style: TextStyle(color: caption,fontSize: 15),),
                    )
                      ],
                    ),
                  );
  }

  Container jadwalHipertensi(Size size) {
    return Container(
                    child: Column(
                      children: [
                        Center(
                    child: Text("Jadwal Hipertensi", style: TextStyle(color: caption, fontSize: 18),),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Jumat", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("18/02/2022", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("07.00", style: TextStyle(color: caption,fontSize: 15),)
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Jumat", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("18/03/2022", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("07.00", style: TextStyle(color: caption,fontSize: 15),)
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Jumat", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("15/04/2022", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("07.00", style: TextStyle(color: caption,fontSize: 15),)
                        ],
                      ),
                    ),
                      ],
                    ),
                  );
  }

  Container jadwalDm(Size size) {
    return Container(
                    child: Column(
                      children: [
                        Center(
                    child: Text("Jadwal DM", style: TextStyle(color: caption, fontSize: 18),),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Jumat", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("11/02/2022", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("07.00", style: TextStyle(color: caption,fontSize: 15),)
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Jumat", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("11/03/2022", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("07.00", style: TextStyle(color: caption,fontSize: 15),)
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Jumat", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("08/04/2022", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("07.00", style: TextStyle(color: caption,fontSize: 15),)
                        ],
                      ),
                    ),
                      ],
                    ),
                  );
  }

  Container pilihRegu(Size size) {
    return Container(
                    child: Column(
                      children: [
                        Center(
                    child: Text("Regu Kelompok", style: TextStyle(color: caption, fontSize: 18),),
                    ),
                    SizedBox(height: 10,),
                    (pilihGol == 1)?Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Text("Dahlia(DM)", style: TextStyle(color: caption,fontSize: 15),),
                    ):Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Text("Raflesia(Hipertensi)", style: TextStyle(color: caption,fontSize: 15),),
                    )
                      ],
                    ),
                  );
  }

  Container pilihGolongan(Size size) {
    return Container(
                    child: Column(
                      children: [
                        Center(
                    child: Text("Pilih Golongan", style: TextStyle(color: caption, fontSize: 18),),
                    ),
                    SizedBox(height: 10,),
                    GestureDetector(
                      onTap:(){
                        setState(() {
                          pilihGol = 1;
                        });
                      },
                      child: Container(
                        alignment: Alignment.center,
                        height: 50,
                        width: size.width,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(15)
                        ),
                        child: Text("DM", style: TextStyle(color: caption,fontSize: 15),),
                      ),
                    ),
                    SizedBox(height: 10,),
                    GestureDetector(
                      onTap:(){
                        setState(() {
                          pilihGol = 2;
                        });
                      },
                      child: Container(
                        alignment: Alignment.center,
                        height: 50,
                        width: size.width,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(15)
                        ),
                        child: Text("Hipertensi", style: TextStyle(color: caption,fontSize: 15),),
                      ),
                    )
                      ],
                    ),
                  );
  }
}