import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_holo_date_picker/flutter_holo_date_picker.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sistemregist/models/golongan/users.dart';
import 'package:sistemregist/views/utils/colors.dart';
import 'package:sistemregist/views/utils/components/field_register.dart';
import 'package:sistemregist/views_models/bloc/bloc_datadiri/datadiri_bloc.dart';
import 'package:sistemregist/views_models/bloc/bloc_golongan/golongan_bloc.dart';

class DataDiri extends StatefulWidget {
  final int? idPengguna;
  final String? email;
  final String? tgl_lahir;
  final String? alamat;
  final String? jenisK;
  const DataDiri({ Key? key, this.idPengguna, this.alamat, this.email, this.jenisK, this.tgl_lahir }) : super(key: key);

  @override
  _DataDiriState createState() => _DataDiriState();
}

class _DataDiriState extends State<DataDiri> {

  DatadiriBloc bloc = DatadiriBloc();
  GolonganBloc blocGol = GolonganBloc();
  TextEditingController _email = TextEditingController();
  TextEditingController _tgl_lahir = TextEditingController();
  TextEditingController _alamat = TextEditingController();
  TextEditingController _jenis_k = TextEditingController();
 

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("sini");
    print(widget.idPengguna);
    blocGol..add(EventGetGolonganById(id: widget.idPengguna));
    
  }


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      floatingActionButton: BlocListener<DatadiriBloc, DatadiriState>(
        bloc: bloc,
        listener: (context, state) {
          // TODO: implement listeneri
          if(state is StateEditDataDiriSukses){
            // changePref();
            blocGol..add(EventGetGolonganById(id: widget.idPengguna));
            // bloc..add(EventGetGolonganById(id: widget.idPengguna));
          }
        },
        child:Container(
          child: BlocBuilder<DatadiriBloc, DatadiriState>(
            bloc: bloc,
            builder: (context, state) {
              if(state is StateEditDataDiriSukses){
                return floatingButton(); 
              }
              if(state is StateEditDataDiriLoading){
                return Container(
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          );
              }
              if(state is StateEditDataDiriFailed){
                return floatingButton(); 
              }
              return floatingButton();
            },
          ),
        )
      ),
      backgroundColor: background,
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        automaticallyImplyLeading: false,
        actions: [
          Container(
            padding: EdgeInsets.only(left: 15,right: 15),
            width: size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Container(
                    height: size.height/20,
                    width: size.width/10,
                    decoration: BoxDecoration(
                      border: Border.all(color: caption),
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: Icon(Icons.arrow_back_ios_new, size: 16,),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: CircleAvatar(
                    child: Icon(Icons.person),
                    radius: size.width / 20,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        child: BlocListener<GolonganBloc, GolonganState>(
          bloc: blocGol,
          listener: (context, state) {
            if(state is StateGetGolonganByIdSukses){
              DateTime tgl = DateTime.parse(state.data!.tglLahir!);
              setState(() {
                _email.text = state.data!.email!;
                _tgl_lahir.text = tgl.toLocal().toString().substring(0,10);
                _jenis_k.text = state.data!.jenisKelamin!;
                _alamat.text = state.data!.alamat!;
              });
            }
            // TODO: implement listener
          },
          child: Container(
            child: BlocBuilder<GolonganBloc, GolonganState>(
          bloc: blocGol,
          builder: (context, state) {
            if(state is StateGetGolonganByIdSukses){
              return formfield(context);
            }
            if(state is StateGetGolonganByIdLoading){
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
            if(state is StateGetGolonganByIdFailed){
              return Container(
                child: Center(
                  child: Text(state.errorMessage.toString()),
                ),
              );
            }
            return Container();
          },
        ),
          ),
        )
      )
    );
  }

  Container formfield(BuildContext context) {
    return Container(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(15),
              child: TextFieldRegister(
                typePass: false,
                readOnly: false,
                              typeInput: false,
                              isiField: _email,
                              hintText: "Email",
                              prefixIcon: Icons.email,
              )
            ),
            Container(
              padding: EdgeInsets.all(15),
              child: TextFieldRegister(
                typePass: false,
                              typeInput: false,
                              readOnly: true,
                              isiField: _tgl_lahir,
                              hintText: "Tanggal Lahir",
                              onTap: () => _showFilter(context),
                              prefixIcon: Icons.date_range_outlined
              )
            ),
            Container(
              padding: EdgeInsets.all(15),
              child: TextFieldRegister(
                typePass: false,
                readOnly: false,
                              typeInput: false,
                              isiField: _alamat,
                              hintText: "Alamat",
                              prefixIcon: Icons.location_city,
              )
            ),
            Container(
              padding: EdgeInsets.all(15),
              child: TextFieldRegister(
                typePass: false,
                readOnly: false,
                              typeInput: false,
                              isiField: _jenis_k,
                              hintText: "Jenis Kelamin (L/P)",
                              prefixIcon: Icons.person,
              )
            ),
          ],
        ),
      );
  }

  _showFilter(BuildContext context) async {
    print("download"); 
    var datePicked = await DatePicker.showSimpleDatePicker(
                context,
                initialDate: DateTime(2022),
                firstDate: DateTime(1960),
                lastDate: DateTime(2023),
                dateFormat: "yyyy-MMMM-dd",
                locale: DateTimePickerLocale.id,
                looping: true,
              );
              final snackBar =
                  SnackBar(content: Text("Date Picked $datePicked"));
                  setState(() {
                    print(datePicked);
                    if(datePicked != null){
                      // datenow = datePicked;
                      _tgl_lahir.text = datePicked.toString().substring(0,10);
                    }
                  });
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  FloatingActionButton floatingButton() {
    return FloatingActionButton(
      onPressed: (){
        UserGolonganModel dataModel = UserGolonganModel(id: widget.idPengguna, email: _email.text,tglLahir: _tgl_lahir.text, alamat: _alamat.text, jenisKelamin: _jenis_k.text);
        bloc..add(EventEditDataDiri(data: dataModel));
      },
      child: Icon(Icons.save),
    );
  }

}