import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sistemregist/models/model_chat/model_chat.dart';
import 'package:sistemregist/views/pages/datadiri/main.dart';
import 'package:sistemregist/views/pages/forum_chat/main.dart';
import 'package:sistemregist/views/pages/home/menu/main.dart';
import 'package:sistemregist/views/pages/login/main.dart';
import 'package:sistemregist/views/pages/pilih_golongan/main.dart';
import 'package:sistemregist/views/pages/registrasiuser/main.dart';
import 'package:sistemregist/views/utils/colors.dart';
import 'package:sistemregist/views/utils/components/text_custom.dart';

class HomePage extends StatefulWidget {
  const HomePage({ Key? key }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String? nama;
  int? myIdAkun;
  int? idPengguna;
  String? email;
  String? tgl_lahir;
  String? alamat;
  String? jenisK;
  int? statusRegist;

   @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  void getData()async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      nama = preferences.getString("nama");
      myIdAkun = preferences.getInt("id_akun");
      idPengguna = preferences.getInt("id_pengguna");
      email = preferences.getString("email");
      tgl_lahir = preferences.getString("tgl_lahir");
      alamat = preferences.getString("alamat");
      jenisK = preferences.getString("jenis_kelamin");
      statusRegist = preferences.getInt("status_regist");
    });
  }

  void _toPage(page){
    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>page));
  }

  void logout()async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
      await preferences.remove("email");
        await preferences.remove("token");
        await preferences.remove("nama");
        await preferences.remove("username");
        await preferences.remove("role");
        await preferences.remove("tgl_lahir");
        await preferences.remove("id_pengguna");
        await preferences.remove("id_akun");
        await preferences.remove("alamat");
        await preferences.remove("jenis_kelamin");
        await preferences.remove("status_regist");
  }
  
  @override
  Widget build(BuildContext context) {
    dynamic size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: background,
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        actions: [
          Expanded(
            child: Container(
              margin: EdgeInsets.all(size.width/20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Home",),
                  Container(
                    child: Row(
                      children: [
                        GestureDetector(
                          onTap: (){
                            logout();
                            Navigator.pop(context);
                            print("sini");
                            
                            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>LoginPage()));
                          },
                          child: Container(
                            child: Icon(Icons.logout),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
          
        ],
      ),
      body: Container(
        margin: EdgeInsets.all(size.width/20),
        child: ListView(
          children: [
            Container(
              width: size.width,
              height:size.height/8,
              // color: Colors.white,
              child: Column(
                children: [
                  textCustom(text: "Hello",color: caption,fontSize: 30,fontWeight: FontWeight.bold,align: Alignment.centerLeft,),
                  textCustom(text: nama.toString(),color: caption,fontSize: 30,fontWeight: FontWeight.bold,align: Alignment.centerLeft,)
                ],
              ),
            ),
            SizedBox(height:10),
            Container(
              width: size.width,
              height:size.height/8,
              // color: Colors.white,
              child: textCustom(text: "Menu",color: caption,fontSize: 20,fontWeight: FontWeight.bold,align: Alignment.centerLeft,),
            ),
            SizedBox(height:5),
            Container(
              alignment: Alignment.topLeft,
              color:Colors.transparent,
              child: WidgetMenu(myIcon1: Icons.data_usage,colorsBackIcon1: Colors.blue,judul1: "Data Diri",myIcon2: Icons.card_membership,colorsBackIcon2: Colors.orange,judul2: "Pilih Golongan",clickCallback2: (){
                _toPage(golongan(idPengguna: idPengguna,));
                },
                clickCallback1: (){
                  _toPage(DataDiri(idPengguna: idPengguna,email: email,tgl_lahir: tgl_lahir,alamat: alamat, jenisK: jenisK,));
                },
                )
            ),
            SizedBox(height: 10,),
            Container(
              alignment: Alignment.topLeft,
              color:Colors.transparent,
              child: WidgetMenu(myIcon1: Icons.app_registration,colorsBackIcon1: Colors.purple,judul1: "Biaya Registrasi",myIcon2: Icons.chat,colorsBackIcon2: Colors.green,judul2: "Konsultasi",
              clickCallback1: (){
                _toPage(BayarRegistrasi(idPengguna: idPengguna));
                },
                clickCallback2: (){
                ChatModel chat = ChatModel(idAkun: myIdAkun);
                _toPage(ForumChat(sourcechat: chat,));
                },)
            )
          ],
        ),
      ),
    );
  }
}