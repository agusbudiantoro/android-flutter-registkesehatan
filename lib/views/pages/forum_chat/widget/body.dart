// import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:sistemregist/models/model_chat/message_model.dart';
// import 'package:sistemregist/models/model_chat/model_chat.dart';
// import 'package:sistemregist/views/pages/forum_chat/widget/own_messages_card.dart';
// import 'package:sistemregist/views/pages/forum_chat/widget/reply_card.dart';
// import 'dart:io';
// import 'package:socket_io_client/socket_io_client.dart' as IO;

// class IndividualPage extends StatefulWidget {
//   const IndividualPage({ Key? key, this.chatModel, this.sourcechat }) : super(key: key);
//   final ChatModel? chatModel;
//   final ChatModel? sourcechat;

//   @override
//   _IndividualPageState createState() => _IndividualPageState();
// }

// class _IndividualPageState extends State<IndividualPage> {
//   bool emojiShowing = false;
//   TextEditingController _pesan = TextEditingController();
//   FocusNode focusNode = FocusNode();
//   ScrollController listController = ScrollController();
//   List<MessageModel> messages=[];

//   IO.Socket? socket;

//   @override
//   void initState() {
//     super.initState();
//     listController = new ScrollController(initialScrollOffset: 1111100.0);
//     connect();
//     focusNode.addListener(() {
//       if(focusNode.hasFocus){
//         setState(() {
//           emojiShowing = false;
//         });
//       }
//     });
//   }

//   void connect(){
//     socket = IO.io("http://192.168.148.71:3000",<String,dynamic>{
//       "transports":["websocket"],
//       "autoConnect":false,
//     });
//     socket!.connect();
//     socket!.emit("signin",widget.sourcechat?.id);
//     socket!.onConnect((data){
//       print("connected");
//       socket!.on("message", (msg){
//         print("cek");
//         print(msg);
//         setMessage("destination", msg["message"]);
//         listController.animateTo(listController.position.maxScrollExtent+50, duration: Duration(milliseconds: 300), curve: Curves.easeOut);
//       });
//     });
//     print(socket!.connected);
//   }

//   void sendMessage(String message,int sourceid, int targetId){
//     setMessage("source", message);
//     socket?.emit("message",{"message":message, "sourceId":sourceid,"targetId":targetId});
    
//   }

//   void setMessage(String type, String message){
//     MessageModel messageModel = MessageModel(type: type, message: message, time:DateTime.now().toString().substring(10,16));
//     setState(() {
//       messages.add(messageModel);
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     var size = MediaQuery.of(context).size;
//     return Scaffold(
//           // resizeToAvoidBottomInset: true,
//           backgroundColor: Colors.transparent,
//           appBar: PreferredSize(
//             preferredSize: Size.fromHeight(60),
//             child: AppBar(
//               leadingWidth: 70,
//               titleSpacing: 0,
//               leading: InkWell(
//                 onTap: (){
//                   Navigator.pop(context);
//                 },
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     Icon(Icons.arrow_back,size: 24,),
//                     CircleAvatar(
//                       radius: 20,
//                       backgroundColor: Colors.grey,
//                       child:  SvgPicture.asset( widget.chatModel!.isGroup! ? "assets/groups.svg":"assets/person.svg",color: Colors.grey[200],height: 37,width: 37,),
//                     )
//                   ],
//                   ),
//               ),
//               title: InkWell(
//                 onTap: (){},
//                 child: Container(
//                   margin: EdgeInsets.all(6),
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.start,
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Text(widget.chatModel!.name!, style: TextStyle(fontSize: 18.5, fontWeight: FontWeight.bold),),
//                       Text("last seen today at 12:05",style: TextStyle(
//                         fontSize: 12
//                       ),)
//                     ],
//                   ),
//                 ),
//               ),
//               actions: [
//                 IconButton(onPressed: (){}, icon: Icon(Icons.videocam)),
//                 IconButton(onPressed: (){}, icon: Icon(Icons.call)),
//                 PopupMenuButton<String>(
//                   onSelected: (value){
//                     print("value");
//                   },
//                   itemBuilder: (BuildContext context){
//                     return [
//                       PopupMenuItem(child: Text("View Contact"), value: "View Contact",),
//                       PopupMenuItem(child: Text("Media, links, and docs"), value: "Media, links, and docs",),
//                       PopupMenuItem(child: Text("Whatsapp Web"), value: "Whatsapp Web",),
//                       PopupMenuItem(child: Text("Search"), value: "Search",),
//                       PopupMenuItem(child: Text("Mute Notification"), value: "Mute Notification",),
//                       PopupMenuItem(child: Text("Walpaper"), value: "Walpaper",),
//                     ];
//                   }
//                   )
//               ],
//             ),
//           ),
//           body: Container(
//             decoration: BoxDecoration(
//               image: DecorationImage(
//                 fit: BoxFit.cover,
//                 image: AssetImage("assets/wa_back.png",)
//               )
//             ),
//             height: size.height,
//             width: size.width,
//             child:Column(
//               children: [
//                 Expanded(
//                   // height: size.height-(size.height/4.6),
//                   child: ListView.builder(
//                     controller: listController,
//                     shrinkWrap: true,
//                     reverse: false,
//                     itemBuilder: (context, index){
//                       if(index == messages.length){
//                         return Container(height:5);
//                       }
//                       if(messages[index].type=="source"){
//                         return OwnMessagesCard(message: messages[index].message,time:messages[index].time);
//                       } else {
//                         return ReplyCard(message: messages[index].message,time:messages[index].time);
//                       }
//                     },
//                     itemCount: messages.length,
//                   ),
//                 ),
//                 Align(
//                   alignment: Alignment.bottomCenter,
//                   child: Container(
//                     height:70,
//                     child: Column(
//                       mainAxisAlignment: MainAxisAlignment.end,
//                       children: [
//                         Row(
//                           children: [
//                             Container(width: size.width-60,child: Card(
//                               margin: EdgeInsets.only(left: 2, right:2, bottom: 8),
//                               shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
//                               child: TextFormField(
//                                 textAlignVertical: TextAlignVertical.center,
//                                 keyboardType: TextInputType.multiline,
//                                 maxLines: 5,
//                                 minLines: 1,
//                                 focusNode: focusNode,
//                                 // autofocus: false,
//                                 controller: _pesan,
//                                 onChanged: (val){
//                                   // print(val);
//                                   // print(_pesan.text);
//                                   setState(() {
//                                     _pesan.text;
//                                   });
//                                 },
//                                 decoration: InputDecoration(
//                                   border: InputBorder.none,
//                                   hintText: "Type a message",
//                                   prefixIcon: IconButton(icon: Icon((emojiShowing)?Icons.keyboard:Icons.emoji_emotions, color: Colors.grey,),onPressed: (){
//                                     setState(() {
//                                       focusNode.unfocus();
//                                       emojiShowing=!emojiShowing;
//                                       if(emojiShowing == false){
//                                         FocusScope.of(context).requestFocus(focusNode);
//                                       }
//                                     });
//                                   },),
//                                   suffixIcon: Row(
//                                     mainAxisSize: MainAxisSize.min,
//                                     children: [
//                                       IconButton(onPressed: (){
//                                         showModalBottomSheet(
//                                           backgroundColor: Colors.transparent,
//                                           context: context, builder: (builder)=>bottomSheet());
//                                       }, icon: Icon(Icons.attach_file,color: Colors.grey,)),
//                                       IconButton(onPressed: (){}, icon: Icon(Icons.camera_alt, color: Colors.grey,))
//                                     ],
//                                   ),
//                                   contentPadding: EdgeInsets.all(5)
//                                 ),
//                               )
//                               )),
//                             Padding(
//                               padding: const EdgeInsets.only(bottom:8, right: 5, left:2),
//                               child: CircleAvatar(
//                                 radius: 25,
//                                 backgroundColor: Color(0xFF128C7E),
//                                 child: IconButton(icon: Icon((_pesan.text.length == 0)?Icons.mic:Icons.send, color: Colors.white,),
//                                 onPressed: (){
//                                   if(_pesan.text.length !=0){
//                                     listController.animateTo(listController.position.maxScrollExtent+50, duration: Duration(milliseconds: 300), curve: Curves.easeOut);
//                                     sendMessage(_pesan.text, widget.sourcechat!.id!, widget.chatModel!.id!);
//                                       _pesan.clear();
//                                   }
//                                 },),
//                               ),
//                             )
//                           ],
//                         ),
//                         emojiSelect()
//                       ],
//                     ),
//                   ),
//                 )
//               ],
//             )
//           ),
//         );
//   }

//   _onEmojiSelected(Emoji emoji) {
//     setState(() {
//       _pesan
//       ..text += emoji.emoji
//       ..selection = TextSelection.fromPosition(
//           TextPosition(offset: _pesan.text.length));
//     });
//   }

//   _onBackspacePressed() {
//     _pesan
//       ..text = _pesan.text.characters.skipLast(1).toString()
//       ..selection = TextSelection.fromPosition(
//           TextPosition(offset: _pesan.text.length));
//   }

//   Widget emojiSelect(){
//     return Offstage(
//               offstage: !emojiShowing,
//               child: SizedBox(
//                 height: 250,
//                 child: EmojiPicker(
//                     onEmojiSelected: (Category category, Emoji emoji) {
//                       _onEmojiSelected(emoji);
//                     },
//                     onBackspacePressed: _onBackspacePressed,
//                     config: Config(
//                         columns: 7,
//                         // Issue: https://github.com/flutter/flutter/issues/28894
//                         emojiSizeMax: 32 * (Platform.isIOS ? 1.30 : 1.0),
//                         verticalSpacing: 0,
//                         horizontalSpacing: 0,
//                         initCategory: Category.RECENT,
//                         bgColor: const Color(0xFFF2F2F2),
//                         indicatorColor: Colors.blue,
//                         iconColor: Colors.grey,
//                         iconColorSelected: Colors.blue,
//                         progressIndicatorColor: Colors.blue,
//                         backspaceColor: Colors.blue,
//                         showRecentsTab: true,
//                         recentsLimit: 28,
//                         noRecentsText: 'No Recents',
//                         noRecentsStyle: const TextStyle(
//                             fontSize: 20, color: Colors.black26),
//                         tabIndicatorAnimDuration: kTabScrollDuration,
//                         categoryIcons: const CategoryIcons(),
//                         buttonMode: ButtonMode.MATERIAL)),
//               ),
//             );
//   }

//   Widget bottomSheet(){
//     return Container(
//       height:278,
//       width: MediaQuery.of(context).size.width,
//       child: Card(
//         margin: EdgeInsets.all(18),
//         child: Padding(
//           padding: const EdgeInsets.symmetric(horizontal: 10, vertical:20),
//           child: Column(
//             children: [
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   iconcreation(Icons.insert_drive_file, Colors.indigo, "Document"),
//                   SizedBox(width: 40,),
//                   iconcreation(Icons.camera_alt, Colors.pink, "Camera"),
//                   SizedBox(width: 40,),
//                   iconcreation(Icons.insert_photo, Colors.purple, "Gallery")
//                 ],
//                 ),
//                 SizedBox(height: 30,),
//                 Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   iconcreation(Icons.headset, Colors.orange, "Audio"),
//                   SizedBox(width: 40,),
//                   iconcreation(Icons.location_pin, Colors.teal, "Location"),
//                   SizedBox(width: 40,),
//                   iconcreation(Icons.person, Colors.blue, "Contact")
//                 ],
//                 )
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   Widget iconcreation(IconData icon, Color color, String text, ){
//     return InkWell(
//       onTap: (){},
//       child: Column(
//         children: [
//           CircleAvatar(
//             backgroundColor: color,
//             radius: 30,
//             child: Icon(icon, size: 29,color: Colors.white,),
//           ),
//           SizedBox(height: 5,),
//           Text(text,style: TextStyle(
//             fontSize: 12
//           ),)
//         ],
//       ),
//     );
//   }
// }