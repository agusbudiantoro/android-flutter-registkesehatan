import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sistemregist/models/model_chat/message_model.dart';
import 'package:sistemregist/models/model_chat/model_chat.dart';
import 'package:sistemregist/views/pages/forum_chat/widget/own_messages_card.dart';
import 'package:sistemregist/views/pages/forum_chat/widget/reply_card.dart';
import 'package:sistemregist/views/utils/colors.dart';
import 'package:sistemregist/views_models/domain.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

class BoxChat extends StatefulWidget {
  const BoxChat({ Key? key,this.chatModel, this.sourcechat }) : super(key: key);
  final ChatModel? chatModel;
  final ChatModel? sourcechat;

  @override
  _BoxChatState createState() => _BoxChatState();
}

class _BoxChatState extends State<BoxChat> {
  bool emojiShowing = false;
  TextEditingController _pesan = TextEditingController();
  FocusNode focusNode = FocusNode();
  ScrollController listController = new ScrollController(initialScrollOffset: 1111100.0);
  List<MessageModel> messages=[];

  IO.Socket? socket= IO.io(urlDomain,<String,dynamic>{
      "transports":["websocket"],
      "autoConnect":false,
    });
    

  @override
  void initState() {
    super.initState();
    
    connect();
    getChat();
    focusNode.addListener(() {
      if(focusNode.hasFocus){
        setState(() {
          emojiShowing = false;
        });
      }
    });
  }

  void connect(){
    
    socket!.connect();
    socket!.emit("signin",widget.sourcechat?.idAkun);
    socket!.onConnect((data){
      print("connected");
      socket!.on("message", (msg){
        print("cek");
        print(msg);
        setMessage(msg['targetId'],msg["sourceId"], msg["message"]);
        print("tes client");
        print(listController.hasClients);
        if(listController.hasClients){
          listController.animateTo(listController.position.maxScrollExtent+50, duration: Duration(milliseconds: 300), curve: Curves.easeOut);
        }
        
      });
    });
    print(socket!.connected);
  }


  void getChat(){
    // print();
    // socket!.on('fromServer', (cek) => print(cek));
    // setMessage("source", message);
    // socket!.on('getchat', (data) => print(data));
    socket!.on("getchat", (msg){
        print("cek2");
        print(msg);
        print("tes1");
        print(json.encode(msg));
        var convertToString =json.encode(msg);
        var listData = jsonDecode(convertToString) as List;
        print("tes");
        print(listData);
        var hasilFinal = listData.map<MessageModel>((item) => MessageModel.fromJson(item)).toList();
        List<MessageModel> hasilConvert=hasilFinal;
        if (mounted) {
          setState(() {
            messages = hasilConvert;
          });
        }
      });
      socket?.emit("getchat",{"targetId":2, "sourceId":widget.sourcechat?.idAkun});
    
  }

  void sendMessage(String message,int sourceid, int targetId){
    setMessage(targetId,sourceid, message);
    socket?.emit("message",{"message":message, "sourceId":sourceid,"targetId":targetId, "waktu":DateTime.now().toString().substring(10,16)});
  }

  void setMessage(int idtarget,int myId, String message){
    MessageModel messageModel = MessageModel(idUserTarget: idtarget,idUser: myId, pesan: message, waktu:DateTime.now().toString().substring(10,16));
    if (mounted) {
    setState(() {
      messages.add(messageModel);
    });
    }
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.all(15),
            
            height: size.height,
            width: size.width,
            child:Column(
              children: [
                Expanded(
                  flex: 20,
                  // height: size.height-(size.height/4.6),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[800],
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: ListView.builder(
                      controller: listController,
                      shrinkWrap: true,
                      reverse: false,
                      itemBuilder: (context, index){
                        if(index == messages.length){
                          return Container(height:5);
                        }
                        if(messages[index].idUser==widget.sourcechat?.idAkun){
                          return OwnMessagesCard(message: messages[index].pesan,time:messages[index].waktu);
                        } else {
                          return ReplyCard(message: messages[index].pesan,time:messages[index].waktu);
                        }
                      },
                      itemCount: messages.length,
                    ),
                  ),
                ),
                Spacer(),
                  Expanded(
                  flex: 2,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[800],
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: Row(
                          children: [
                            Expanded(
                              flex:5,
                              child: Container(
                                padding: const EdgeInsets.only(left:8.0, right:8),
                                child: TextFormField(
                                  style: TextStyle(color: caption),
                                  cursorColor: caption,
                                textAlignVertical: TextAlignVertical.center,
                                keyboardType: TextInputType.multiline,
                                maxLines: 5,
                                minLines: 1,
                                focusNode: focusNode,
                                // autofocus: false,
                                controller: _pesan,
                                onChanged: (val){
                                  // print(val);
                                  // print(_pesan.text);
                                  setState(() {
                                    _pesan.text;
                                  });
                                },
                                decoration: InputDecoration(
                                  
                                  // col
                                  border: InputBorder.none,
                                  hintText: "Type a message",
                                  hintStyle: TextStyle(color: caption)
                                  
                                  // contentPadding: EdgeInsets.all(5)
                                ),
                            ),
                              )),
                            Expanded(
                              flex: 2,
                              child: Container(
                                margin: EdgeInsets.only(right: 5),
                                height: size.height/25,
                                decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius: BorderRadius.circular(20)
                                ),
                                child: TextButton(
                                  child: Text("Send",style: TextStyle(color: caption), ),
                                onPressed: (){
                                  if(_pesan.text.length !=0){
                                    listController.animateTo(listController.position.maxScrollExtent+50, duration: Duration(milliseconds: 300), curve: Curves.easeOut);
                                    sendMessage(_pesan.text, widget.sourcechat!.idAkun!, 2);
                                      _pesan.clear();
                                  }
                                },),
                              ),
                            )
                          ],
                        ),
                  )),
                // Align(
                //   alignment: Alignment.bottomCenter,
                //   child: Container(
                //     height:70,
                //     child: Column(
                //       mainAxisAlignment: MainAxisAlignment.end,
                //       children: [
                //         Row(
                //           children: [
                //             Container(width: size.width-60,child: Card(
                //               margin: EdgeInsets.only(left: 2, right:2, bottom: 8),
                //               shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                //               child: TextFormField(
                //                 textAlignVertical: TextAlignVertical.center,
                //                 keyboardType: TextInputType.multiline,
                //                 maxLines: 5,
                //                 minLines: 1,
                //                 focusNode: focusNode,
                //                 // autofocus: false,
                //                 controller: _pesan,
                //                 onChanged: (val){
                //                   // print(val);
                //                   // print(_pesan.text);
                //                   setState(() {
                //                     _pesan.text;
                //                   });
                //                 },
                //                 decoration: InputDecoration(
                //                   border: InputBorder.none,
                //                   hintText: "Type a message",
                //                   prefixIcon: IconButton(icon: Icon((emojiShowing)?Icons.keyboard:Icons.emoji_emotions, color: Colors.grey,),onPressed: (){
                //                     setState(() {
                //                       focusNode.unfocus();
                //                       emojiShowing=!emojiShowing;
                //                       if(emojiShowing == false){
                //                         FocusScope.of(context).requestFocus(focusNode);
                //                       }
                //                     });
                //                   },),
                //                   suffixIcon: Row(
                //                     mainAxisSize: MainAxisSize.min,
                //                     children: [
                //                       IconButton(onPressed: (){
                //                         showModalBottomSheet(
                //                           backgroundColor: Colors.transparent,
                //                           context: context, builder: (builder)=>bottomSheet());
                //                       }, icon: Icon(Icons.attach_file,color: Colors.grey,)),
                //                       IconButton(onPressed: (){}, icon: Icon(Icons.camera_alt, color: Colors.grey,))
                //                     ],
                //                   ),
                //                   contentPadding: EdgeInsets.all(5)
                //                 ),
                //               )
                //               )),
                //             Padding(
                //               padding: const EdgeInsets.only(bottom:8, right: 5, left:2),
                //               child: CircleAvatar(
                //                 radius: 25,
                //                 backgroundColor: Color(0xFF128C7E),
                //                 child: IconButton(icon: Icon((_pesan.text.length == 0)?Icons.mic:Icons.send, color: Colors.white,),
                //                 onPressed: (){
                //                   if(_pesan.text.length !=0){
                //                     listController.animateTo(listController.position.maxScrollExtent+50, duration: Duration(milliseconds: 300), curve: Curves.easeOut);
                //                     sendMessage(_pesan.text, widget.sourcechat!.id!, widget.chatModel!.id!);
                //                       _pesan.clear();
                //                   }
                //                 },),
                //               ),
                //             )
                //           ],
                //         ),
                //         emojiSelect()
                //       ],
                //     ),
                //   ),
                // )
              ],
            )
          );
  }


  _onBackspacePressed() {
    _pesan
      ..text = _pesan.text.characters.skipLast(1).toString()
      ..selection = TextSelection.fromPosition(
          TextPosition(offset: _pesan.text.length));
  }


  Widget bottomSheet(){
    return Container(
      height:278,
      width: MediaQuery.of(context).size.width,
      child: Card(
        margin: EdgeInsets.all(18),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical:20),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  iconcreation(Icons.insert_drive_file, Colors.indigo, "Document"),
                  SizedBox(width: 40,),
                  iconcreation(Icons.camera_alt, Colors.pink, "Camera"),
                  SizedBox(width: 40,),
                  iconcreation(Icons.insert_photo, Colors.purple, "Gallery")
                ],
                ),
                SizedBox(height: 30,),
                Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  iconcreation(Icons.headset, Colors.orange, "Audio"),
                  SizedBox(width: 40,),
                  iconcreation(Icons.location_pin, Colors.teal, "Location"),
                  SizedBox(width: 40,),
                  iconcreation(Icons.person, Colors.blue, "Contact")
                ],
                )
            ],
          ),
        ),
      ),
    );
  }

  Widget iconcreation(IconData icon, Color color, String text, ){
    return InkWell(
      onTap: (){},
      child: Column(
        children: [
          CircleAvatar(
            backgroundColor: color,
            radius: 30,
            child: Icon(icon, size: 29,color: Colors.white,),
          ),
          SizedBox(height: 5,),
          Text(text,style: TextStyle(
            fontSize: 12
          ),)
        ],
      ),
    );
  }
}