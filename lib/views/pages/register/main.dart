
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sistemregist/views/utils/colors.dart';
import 'package:sistemregist/views/utils/components/button_regist.dart';
import 'package:sistemregist/views/utils/components/field_register.dart';
import 'package:sistemregist/views_models/bloc/bloc_register/register_bloc.dart';

import '../../../models/model_register/form_register_user.dart';


class RegisterMain extends StatefulWidget {
  const RegisterMain({ Key? key }) : super(key: key);

  @override
  _RegisterMainState createState() => _RegisterMainState();
}

class _RegisterMainState extends State<RegisterMain> {
  final TextEditingController _passWord = TextEditingController();
  final TextEditingController _nama_user = TextEditingController();
  final TextEditingController _user_name = TextEditingController();

  RegisterBloc bloc = RegisterBloc();
  bool statusPass = true;
  void _clickCallBack() {
    setState(() {
      statusPass = !statusPass;
    });
  }

  @override
  void initState() {
    super.initState();
    // bloc..add(BlocEventDisconnect());
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: background,
      body: Container(
        height: size.height,
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          physics: AlwaysScrollableScrollPhysics(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              // height: size.height,
                              padding:const EdgeInsets.symmetric(vertical: 70),
                        alignment: Alignment.center,
                        child: Column(
                            children: [
                              // Image.asset("assets/logo/logo.png", height: 200,),
                              const Text(
                                "Register",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 30.0,
                                    fontWeight: FontWeight.bold,fontFamily: 'RobotoCondensed'),
                              ),
                              SizedBox(height:10),
                              // Spacer(),
                              TextFieldRegister(
                                readOnly: false,
                                typePass: false,
                                typeInput: false,
                                isiField: _user_name,
                                hintText: "Username",
                                prefixIcon: Icons.person,
                              ),
                              TextFieldRegister(
                                readOnly: false,
                                typePass: false,
                                typeInput: false,
                                isiField: _nama_user,
                                hintText: "Nama Lengkap",
                                prefixIcon: Icons.person_pin,
                              ),
                              TextFieldRegister(
                                readOnly: false,
                                typePass: true,
                                typeInput: false,
                                  isiField: _passWord,
                                  hintText: "Password",
                                  prefixIcon: Icons.vpn_key_rounded,
                                  isObsercure: statusPass,
                                  suffixIcon: Icons.remove_red_eye,
                                  clickCallback: () => _clickCallBack()),
                              SizedBox(height: 20,),
                              Container(
                                width: size.width,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    BlocListener<RegisterBloc, RegisterState>(
                                    bloc: bloc,
                                    listener: (context, state) {
                                      if(state is BlocStateSukses){
                                        ScaffoldMessenger.of(context).showSnackBar(
                                          SnackBar(
                                            content: Text("Register Berhasil"),
                                            duration: Duration(milliseconds: 500),
                                            backgroundColor: Colors.green,
                                            onVisible: (){
                                              // Navigator.push(context, MaterialPageRoute(builder: (context)=>BackgroundHome()));
                                            },
                                          )
                                        );
                                        Future.delayed(Duration(milliseconds: 502), () => Navigator.pop(context));
                                      }
                                      if(state is BlocaStateFailed){
                                        ScaffoldMessenger.of(context).showSnackBar(
                                          SnackBar(
                                            content: Text("Register Gagal, "+state.errorMessage.toString()),
                                            duration: Duration(milliseconds: 500),
                                            backgroundColor: Colors.red,
                                          )
                                        );
                                      }
                                    },
                                    child: Container(
                                      child: BlocBuilder<RegisterBloc, RegisterState>(
                                        bloc: bloc,
                                        builder: (context, state) {
                                          if (state is BlocStateSukses) {
                                            return buildButtonRegister();
                                          }
                                          if (state is BlocStateLoading) {
                                            return Center(
                                              child: CircularProgressIndicator(color: Colors.white,),
                                            );
                                          }
                                          if (state is BlocaStateFailed) {
                                            return buildButtonRegister();
                                          }
                                          return buildButtonRegister();
                                        },
                                      ),
                                    ),
                                  ),
                                  ButtonRegister(
                                    onPress: (){
                                      registUser formData = registUser(name: _nama_user.text, password: _passWord.text, username: _user_name.text);
                                      bloc..add(BlocEventRegister(myData: formData));
                                      // print("coab");
                                      // Navigator.push(context, MaterialPageRoute(builder: (context)=>PageChat(id:2)));
                                      // Navigator.push(context, MaterialPageRoute(builder: (context)=>ListChat()));
                                    },
                                    buttonName: "Daftar",
                                    paddingH: 35.0,
                                  )
                                  ],
                                ),
                              ),
                              // Spacer()
                            ],
                          ),
                      ),
          ],
            ),
        ),
      ),
    );
  }

  ButtonRegister buildButtonRegister() {
    return ButtonRegister(
      onPress: (){
        print("sign");
        Navigator.pop(context);
      },
      buttonName: "Kembali",
      paddingH: 35.0,
    );
  }
}