import 'package:flutter/material.dart';
import 'package:sistemregist/views/utils/colors.dart';

class WidgetMenuAdmin extends StatefulWidget {
  IconData? myIcon1;
  String? judul1;
  Color? colorsBackIcon1;
  IconData? myIcon2;
  String? judul2;
  Color? colorsBackIcon2;
  final VoidCallback? clickCallback;

  WidgetMenuAdmin({this.myIcon1,this.judul1,this.colorsBackIcon1, this.myIcon2, this.colorsBackIcon2,this.judul2, this.clickCallback});

  @override
  _WidgetMenuAdminState createState() => _WidgetMenuAdminState();
}

class _WidgetMenuAdminState extends State<WidgetMenuAdmin> {
  @override
  Widget build(BuildContext context) {
    var size=MediaQuery.of(context).size;
    return GestureDetector(
      onTap: (){
        widget.clickCallback!();
      },
      child: Container(
        child: Row(
          children: [
            Expanded(
              flex:1,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.grey[800],
                  borderRadius: BorderRadius.circular(10),
                ),
                height: size.height/6,
                child: Column(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        color: Colors.transparent,
                        child: Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Container(
                                padding: EdgeInsets.all(10),
                                alignment: Alignment.topLeft,
                                child: Container(
                                  padding: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                    borderRadius:BorderRadius.circular(10),
                                    color:widget.colorsBackIcon1
                                  ),
                                  child: Icon(widget.myIcon1, color:caption,),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                color:Colors.transparent
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        padding: EdgeInsets.all(10),
                        alignment: Alignment.topLeft,
                        child: Text(widget.judul1!,style:TextStyle(color: caption, fontWeight: FontWeight.bold)),
                        color: Colors.transparent,
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(width:10),
            Expanded(
              flex:1,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.grey[800],
                  borderRadius: BorderRadius.circular(10),
                ),
                height: size.height/6,
                child: Column(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        color: Colors.transparent,
                        child: Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Container(
                                padding: EdgeInsets.all(10),
                                alignment: Alignment.topLeft,
                                child: Container(
                                  padding: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                    borderRadius:BorderRadius.circular(10),
                                    color:widget.colorsBackIcon2
                                  ),
                                  child: Icon(widget.myIcon2, color:caption,),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                color:Colors.transparent
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        padding: EdgeInsets.all(10),
                        alignment: Alignment.topLeft,
                        child: Text(widget.judul2!,style:TextStyle(color: caption, fontWeight: FontWeight.bold)),
                        color: Colors.transparent,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}