import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sistemregist/models/golongan/users.dart';
import 'package:sistemregist/models/model_login/model_respone_login.dart';
import 'package:sistemregist/views/pages_dokter/forum_chat/widget/button/home/buttoncustom1.dart';
import 'package:sistemregist/views/utils/colors.dart';
import 'package:sistemregist/views/utils/components/dialog_delete/dialog_delete.dart';
import 'package:sistemregist/views/utils/components/dialog_verif/dialog_verif.dart';
import 'package:sistemregist/views_models/bloc/bloc_akun/akun_bloc.dart';
import 'package:sistemregist/views_models/bloc/bloc_datadiri/datadiri_bloc.dart';
import 'package:sistemregist/views_models/bloc/bloc_golongan/golongan_bloc.dart';


class PagePesertaAdmin extends StatefulWidget {
  const PagePesertaAdmin({ Key? key }) : super(key: key);

  @override
  _PagePesertaAdminState createState() => _PagePesertaAdminState();
}

class _PagePesertaAdminState extends State<PagePesertaAdmin> {
  AkunBloc bloc = AkunBloc();
  GolonganBloc blocGol = GolonganBloc();
  int? jumlahJadwal;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc..add(EventGetAllAkun());
  }

  @override
  Widget build(BuildContext context) {
    var size= MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: background,
      body: Container(
        color: Colors.transparent,
        margin: EdgeInsets.all(20),
      child: Column(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.only(top:size.height/16),
              color: Colors.transparent,
              child: Row(
                children: [
                  Expanded(
                    flex: 3,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text("Daftar", style: TextStyle(color: caption, fontSize: 30, fontWeight: FontWeight.normal),)),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text("Peserta", style: TextStyle(color: caption, fontSize: 30, fontWeight: FontWeight.bold),))
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 4,
            child: Container(
              width: size.width,
              decoration: BoxDecoration(
                            color: Colors.transparent, 
                            borderRadius: BorderRadius.circular(20)),
              child: BlocListener<AkunBloc, AkunState>(
                bloc: bloc,
                listener: (context, state) {
                  // TODO: implement listener
                  if(state is StateGetAllAkunSukses){
                    // setState(() {
                      // jumlahJadwal=state.listData!.length;
                    // });
                  }
                },
                child: Container(
                  child: BlocBuilder<AkunBloc, AkunState>(
                bloc: bloc,
                builder: (context, state) {
                  if(state is StateGetAllAkunSukses){
                    print("cek");
                    return ListViewChild(size, state.data);
                  }
                  if(state is StateGetAllAkunLoading){
                    return Container(child: Center(child: CircularProgressIndicator(),),);
                  }
                  if(state is StateGetAllAkunFailed){
                    return Container(child: Center(child: Text("Tidak dapat menampilkan data"),),);
                  }
                  return Container();
                },
              ),
                ),
              )
            ),
          )
        ],
      ),
    ),
    );
  }

  ListView ListViewChild(Size size, List<ModelResponseLogin>? data) {
    return ListView.builder(
              shrinkWrap: true,
              physics: AlwaysScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemCount: data!.length,
              itemBuilder: (BuildContext context, int i){
                return Container(
                  margin: EdgeInsets.all(5),
                  height: size.height/6.8,
                  width: size.width,
                  decoration: BoxDecoration(
                        border: Border.all(color: caption, width: 2),
                          color: background, 
                          borderRadius: BorderRadius.circular(20)),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 4,
                                child: Container(
                                  margin: EdgeInsets.all(15),
                                  // color: Colors.transparent,
                                  child: Column(
                                    children: [
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text("Nama : "+data[i].name.toString(), style: TextStyle(color: caption, fontSize: 16, fontWeight: FontWeight.bold))),
                                        ),
                                        Expanded(
                                          flex:2,
                                          child: Row(
                                            children: [
                                    // Container(
                                    //   // flex: 1,
                                    //   child: BlocListener<GolonganBloc, GolonganState>(
                                    //     bloc: blocGol,
                                    //     listener: (context, state) {
                                    //       // TODO: implement listener
                                    //       if(state is StateVerifGolonganByIdSukses){
                                    //         print("sukses");
                                    //         bloc..add(EventGetAllAkun());
                                    //       }
                                    //     },
                                    //     child: Container(
                                    //       child: BlocBuilder<GolonganBloc, GolonganState>(
                                    //     bloc: blocGol,
                                    //     builder: (context, state) {
                                    //       if(state is StateVerifGolonganByIdFailed){
                                    //         return button1(sizeIcon: 13,sizeText: 12,height: 25, width: 6,name: "Verif",collorName: caption,iconButton: Icons.check_sharp,collorIcon: Colors.green,collorButton: caption,borderRadius: 88,fillCollor: Colors.transparent, clickCallback: ()=>_clickCallBackVerif(data[i].idPengguna!),);
                                    //       }
                                    //       if(state is StateVerifGolonganByIdLoading){
                                    //         return Container(
                                    //           child: Center(
                                    //             child: CircularProgressIndicator(),
                                    //           ),
                                    //         );
                                    //       }
                                    //       if(state is StateVerifGolonganByIdSukses){
                                    //         return button1(sizeIcon: 13,sizeText: 12,height: 25, width: 6,name: "Verif",collorName: caption,iconButton: Icons.check_sharp,collorIcon: Colors.green,collorButton: caption,borderRadius: 88,fillCollor: Colors.transparent, clickCallback: ()=>_clickCallBackVerif(data[i].idPengguna!),);
                                    //       }
                                    //       return button1(sizeIcon: 13,sizeText: 12,height: 25, width: 6,name: "Verif",collorName: caption,iconButton: Icons.check_sharp,collorIcon: Colors.green,collorButton: caption,borderRadius: 88,fillCollor: Colors.transparent, clickCallback: ()=>_clickCallBackVerif(data[i].idPengguna!),);
                                    //     },
                                    //   ),
                                    //     ),
                                    //   )
                                    // ),
                                    Container(
                                      // flex: 1,
                                      child: BlocListener<AkunBloc, AkunState>(
                                        bloc: bloc,
                                        listener: (context, state) {
                                          // TODO: implement listener
                                          if(state is StateDeleteAkunSukses){
                                            print("sukses");
                                            bloc..add(EventGetAllAkun());
                                          }
                                        },
                                        child: Container(
                                          child: BlocBuilder<AkunBloc, AkunState>(
                                        bloc: bloc,
                                        builder: (context, state) {
                                          if(state is StateDeleteAkunFailed){
                                            return button1(sizeIcon: 13,sizeText: 12,height: 25, width: 6,name: "Delete",collorName: caption,iconButton: Icons.remove,collorIcon: Colors.red,collorButton: caption,borderRadius: 88,fillCollor: Colors.transparent,clickCallback: ()=>_clickCallBackDelete(data[i].id!));
                                          }
                                          if(state is StateDeleteAkunLoading){
                                            return Container(
                                              child: Center(
                                                child: CircularProgressIndicator(),
                                              ),
                                            );
                                          }
                                          if(state is StateDeleteAkunSukses){
                                            return button1(sizeIcon: 13,sizeText: 12,height: 25, width: 6,name: "Delete",collorName: caption,iconButton: Icons.remove,collorIcon: Colors.red,collorButton: caption,borderRadius: 88,fillCollor: Colors.transparent,clickCallback: ()=>_clickCallBackDelete(data[i].id!));
                                          }
                                          return button1(sizeIcon: 13,sizeText: 12,height: 25, width: 6,name: "Delete",collorName: caption,iconButton: Icons.remove,collorIcon: Colors.red,collorButton: caption,borderRadius: 88,fillCollor: Colors.transparent,clickCallback: ()=>_clickCallBackDelete(data[i].id!));
                                        },
                                      ),
                                        ),
                                      )
                                    ),
                                           ],
                                          ),
                                        )
                                    ],
                                  ),
                                )),
                              //   Expanded(
                              //   flex: 2,
                              //   child: Column(
                              //     children: [
                                    
                              //     ],
                              //   ),
                              // ),
                                // Expanded(
                                // flex: 1,
                                // child: Container(
                                //   decoration: BoxDecoration(
                                //   // border: Border.all(color: blackMetalic, width: 2),
                                //     color: (data[i].status == 1)?Colors.green:Colors.red, 
                                //     borderRadius: BorderRadius.only(topRight: Radius.circular(20),bottomRight: Radius.circular(20))),
                                //   child: Column(
                                //     mainAxisAlignment: MainAxisAlignment.center,
                                //     children: [
                                //       (data[i].status == 1)?Icon(Icons.check_circle_outline_sharp, color: Colors.white, size: 30,):Icon(Icons.cancel_rounded, color: Colors.white, size: 30,)
                                //     ],
                                //   )
                                // ))
                            ],
                          ),
                );
              }
              );
  }

  Future<void> _clickCallBackDelete(int id) {
    return showCupertinoDialog(
              context: context,
              builder: (BuildContext context) => DialogDelete(id:id,callback: callbackDelete,)
            );
  }

  callbackDelete(int id){
    print("delete");
    print(id);
    UserGolonganModel mydata = UserGolonganModel(id: id);
    bloc..add(EventDeleteAkun(data: mydata));
  }

  Future<void> _clickCallBackVerif(int id) {
    return showCupertinoDialog(
              context: context,
              builder: (BuildContext context) => DialogVerif(id:id,callback: callbackVerif,)
            );
  }

  callbackVerif(int id){
    print("delete");
    print(id);
    // blocVerif..add(EventVerifUser(id: id));
  }
}