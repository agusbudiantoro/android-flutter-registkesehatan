import 'package:flutter/material.dart';
import 'package:sistemregist/views/utils/colors.dart';

class PageJadwal extends StatefulWidget {
  const PageJadwal({ Key? key }) : super(key: key);

  @override
  _PageJadwalState createState() => _PageJadwalState();
}

class _PageJadwalState extends State<PageJadwal> {
  @override
  Widget build(BuildContext context) {
    var size= MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: background,
      appBar: AppBar(
        backgroundColor: background,
      ),
      body: Column(
        children: [
          jadwalHipertensi(size),
          SizedBox(height: 10,),
          jadwalDm(size)
        ],
      ),
    );
  }

  Container jadwalHipertensi(Size size) {
    return Container(
                    child: Column(
                      children: [
                        Center(
                    child: Text("Jadwal Hipertensi", style: TextStyle(color: caption, fontSize: 18),),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Jumat", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("18/02/2022", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("07.00", style: TextStyle(color: caption,fontSize: 15),)
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Jumat", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("18/03/2022", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("07.00", style: TextStyle(color: caption,fontSize: 15),)
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Jumat", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("15/04/2022", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("07.00", style: TextStyle(color: caption,fontSize: 15),)
                        ],
                      ),
                    ),
                      ],
                    ),
                  );
  }

  Container jadwalDm(Size size) {
    return Container(
                    child: Column(
                      children: [
                        Center(
                    child: Text("Jadwal DM", style: TextStyle(color: caption, fontSize: 18),),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Jumat", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("11/02/2022", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("07.00", style: TextStyle(color: caption,fontSize: 15),)
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Jumat", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("11/03/2022", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("07.00", style: TextStyle(color: caption,fontSize: 15),)
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Jumat", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("08/04/2022", style: TextStyle(color: caption,fontSize: 15),),
                          SizedBox(width: 5,),
                          Text("07.00", style: TextStyle(color: caption,fontSize: 15),)
                        ],
                      ),
                    ),
                      ],
                    ),
                  );
  }
}